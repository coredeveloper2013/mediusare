<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careers', function (Blueprint $table) {
            $table->increments('id');
            $table->text('slug');
            $table->string('title');
            $table->tinyInteger('vacancy')->nullable();
            $table->text('job_context')->nullable();
            $table->longText('job_responsibilities')->nullable();
            $table->string('employment_status')->nullable();
            $table->text('experience_requirements')->nullable();
            $table->text('educational_requirements')->nullable();
            $table->longText('additional_requirements')->nullable();
            $table->longText('other_benefits')->nullable();
            $table->longText('read_before_apply')->nullable();
            $table->string('salary')->nullable();
            $table->date('deadline')->nullable();
            $table->string('apply_email')->nullable();
            $table->string('image')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careers');
    }
}
