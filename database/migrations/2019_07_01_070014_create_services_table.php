<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->text('slug');
            $table->string('title');
            $table->text('short_description');
            $table->longText('description_left');
            $table->longText('description_right');
            $table->string('image', 32)->nullable();
            $table->string('industries_image', 32)->nullable();
            $table->string('icon', 32)->nullable();
            $table->date('date')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('inactive');
            $table->integer('sort')->default(0);
            $table->timestamps();
        });

        Schema::create('service_offered', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id');
            $table->string('offered_services_title');
            $table->timestamps();
        });

        Schema::create('service_industries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id');
            $table->string('services_industries_title');
            $table->timestamps();
        });

        Schema::create('service_technology_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id');
            $table->integer('technology_id');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
        Schema::dropIfExists('services_offered');
        Schema::dropIfExists('services_industries');
        Schema::dropIfExists('services_technology_relations');
    }
}
