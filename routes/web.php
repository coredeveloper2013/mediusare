<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/sendCorn', 'BackupMySqlController@sendCorn');
Route::post('/login', 'Auth\LoginController@login');
Auth::routes();

//Admin Route
Route::group(['prefix' => 'mediuswareadmin', 'middleware' => 'auth'], function () {
    Route::get('dashboard', 'Admin\DashboardController@index')->name('home');
    //services
    Route::resource('services', 'Admin\ServicesController');
    Route::post('service/sortable', 'Admin\ServicesController@sortable');
    //category
    Route::resource('category', 'Admin\CategoryController');
    //portfolio
    Route::resource('portfolio', 'Admin\PortfolioController');
    Route::post('portfolio/sortable', 'Admin\PortfolioController@sortable');
    Route::get('portfolio/image/{id}/delete', 'Admin\PortfolioController@portfolioImageDelete');
    //team
    Route::resource('team', 'Admin\TeamController');
    Route::post('team/sortable', 'Admin\TeamController@sortable');
    //slider
    Route::resource('slider', 'Admin\SliderController');
    Route::post('slider/sortable', 'Admin\SliderController@sortable');
    //feature
    Route::resource('feature', 'Admin\FeatureController');
    //setting
    Route::get('setting', 'Admin\DashboardController@setting');
    Route::post('setting/store', 'Admin\DashboardController@settingStore');
    //career
    Route::resource('careers', 'Admin\CareerController');
    //contact
    Route::get('contacts', 'Admin\DashboardController@contacts');
    Route::get('contact/delete/{id}', 'Admin\DashboardController@contactDelete');
    //about
    Route::resource('about', 'Admin\AboutController');
    Route::post('about/sortable', 'Admin\AboutController@sortable');
    //technology
    Route::resource('technology', 'Admin\TechnologyController');
    Route::post('technology/sortable', 'Admin\TechnologyController@sortable');
    //client
    Route::resource('client', 'Admin\ClientController');
    Route::post('client/sortable', 'Admin\ClientController@sortable');
    //client
    Route::resource('partner', 'Admin\PartnerController');
    Route::post('partner/sortable', 'Admin\PartnerController@sortable');
    //informative
    Route::resource('informative-text', 'Admin\InformativeTextController');
    //informative
    Route::resource('testimonial', 'Admin\TestimonialController');
    Route::get('profile', 'Admin\ProfileController@index')->name('profile.index');
    Route::post('profile-update', 'Admin\ProfileController@updateProfile')->name('profile.update');
    Route::post('updatepassword', 'Admin\ProfileController@updatePassword')->name('updatePassword');

    Route::group(['middleware' => 'admin'], function () {
        //user
        Route::resource('user', 'Admin\UserController');
        Route::post('user/sortable', 'Admin\UserController@sortable');
        Route::resource('db-backup', 'Admin\BackupController');
    });

});

//Frontend Route
Route::get('/get-abouts', 'WebsiteController@getAbouts');
Route::get('/get-portfolios-showcase', 'WebsiteController@getPortfolioShowcase');
Route::get('/get-portfolios', 'WebsiteController@getPortfolios');
Route::get('/get-portfolios/{slug}', 'WebsiteController@getPortfolios');
Route::get('/get-portfolio-details/{slug}', 'WebsiteController@getPortfolioDetails');
Route::get('/get-categories', 'WebsiteController@getCategories');
Route::get('/get-features', 'WebsiteController@getFeatures');
Route::get('/get-feature-details/{slug}', 'WebsiteController@getFeatureDetails');
Route::get('/get-services', 'WebsiteController@getServices');
Route::get('/get-service-details/{slug}', 'WebsiteController@getServiceDetails');
Route::get('/get-setting', 'WebsiteController@getSettingInfo');
Route::get('/get-teams', 'WebsiteController@getTeam');
Route::get('/get-team-details/{slug}', 'WebsiteController@getTeamDetails');
Route::post('/contact-store', 'WebsiteController@contactStore');
Route::post('/team-contact-store', 'WebsiteController@teamContactStore');
Route::post('/newsletter-store', 'WebsiteController@newsletterStore');
Route::get('/get-technology', 'WebsiteController@getTechnology');
Route::get('/get-clients', 'WebsiteController@getClients');
Route::get('/get-sliders', 'WebsiteController@getSliders');
Route::get('/get-informative-text', 'WebsiteController@getInformativeText');
Route::get('/get-testimonials', 'WebsiteController@getTestimonials');
Route::get('/get-partners', 'WebsiteController@getPartners');
Route::get('/get-careers', 'WebsiteController@getCareers');
Route::get('/get-career-details/{slug}', 'WebsiteController@getCareerDetails');


Route::get('/{vue_route?}', 'WebsiteController@home')->where('vue_route', '(.*)');
