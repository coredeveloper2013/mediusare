const mix = require('laravel-mix');
let path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const resourcesAssets = 'resources/';
const publicAssets = 'public/assets/';

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
mix.sass(resourcesAssets + 'sass/custom.scss', publicAssets+'css/custom.css');

//All css added
mix.styles([
    publicAssets+'fbmsg.css',
    publicAssets+'css/linearicons.css',
    publicAssets+'css/font-awesome.min.css',
    publicAssets+'css/bootstrap.css',
    publicAssets+'css/magnific-popup.css',
    publicAssets+'css/nice-select.css',
    publicAssets+'css/animate.min.css',
    publicAssets+'css/owl.carousel.css',
    publicAssets+'css/animate.css',
    publicAssets+'css/main.css',
], publicAssets+'css/all.css');


//All js added
mix.scripts([
    publicAssets+'js/vendor/jquery-2.2.4.min.js',
    publicAssets+'js/popper.min.js',
    publicAssets+'js/vendor/bootstrap.min.js',
    publicAssets+'js/vendor/jquery.vide.min.js',
    publicAssets+'js/easing.min.js',
    publicAssets+'js/superfish.min.js',
    publicAssets+'js/jquery.ajaxchimp.min.js',
    publicAssets+'js/mail-script.js',
    publicAssets+'js/wow.js"',
    publicAssets+'js/youtube.min.js',
    publicAssets+'fbmsg.js',
    publicAssets+'js/owl.carousel.min.js',
], publicAssets+'js/all.js');
