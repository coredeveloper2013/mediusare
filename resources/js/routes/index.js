import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// import index     from '../components/frontend/index'
import home               from '../components/frontend/home'
import features           from '../components/frontend/features'
import featureDetails     from '../components/frontend/feature-details'
import teams              from '../components/frontend/teams'
import teamDetails        from '../components/frontend/team-details'
import portfolios         from '../components/frontend/portfolios'
import portfoliosCategory from '../components/frontend/portfolios-category'
import portfoliosShowcase from '../components/frontend/portfolios-showcase'
import portfolioDetails   from '../components/frontend/portfolio-details'
import services           from '../components/frontend/services'
import serviceDetails     from '../components/frontend/service-details'
import about              from '../components/frontend/about'
import contact            from '../components/frontend/contact'
import careers            from '../components/frontend/careers'
import careerDetails      from '../components/frontend/career-details'
import ourPartner         from '../components/frontend/our-partner'
import pageNotFound       from '../components/frontend/404.vue'

export default new Router({
    mode: 'history',
    routes: [
        // {path: '/',               component: index, name: 'index'},
        {path: '/',                         component: home, name: 'home'},
        // {path: '/home',                     component: home, name: 'home'},
        {path: '/features',                 component: features, name: 'features'},
        {path: '/feature/:slug',            component: featureDetails, name: 'featureDetails'},
        {path: '/teams',                    component: teams, name: 'teams'},
        {path: '/team/:slug',               component: teamDetails, name: 'teamDetails'},
        {path: '/portfolios',               component: portfolios, name: 'portfolios'},
        {path: '/portfolios/:slug',         component: portfolios, name: 'portfolios'},
        {path: '/portfolios-showcase',      component: portfoliosShowcase, name: 'portfoliosShowcase'},
        {path: '/portfolio/:slug',          component: portfolioDetails, name: 'portfolioDetails'},
        {path: '/services',                 component: services, name: 'services'},
        {path: '/service/:slug',            component: serviceDetails, name: 'serviceDetails'},
        {path: '/about',                    component: about, name: 'about'},
        {path: '/contact',                  component: contact, name: 'contact'},
        {path: '/careers',                  component: careers, name: 'careers'},
        {path: '/career-details/:slug',     component: careerDetails, name: 'careerDetails'},
        {path: '/our-partner',              component: ourPartner, name: 'ourPartner'},
        {path: "*",                         component: pageNotFound, name: 'pageNotFound'}
    ],
});
