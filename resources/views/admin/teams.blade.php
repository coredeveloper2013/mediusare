@extends('admin.master')

@section('title')
    Mediusware | Teams
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">teams</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Teams</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

    @include('admin.include.alert')

        <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">All Team</h4>
                            </div>
                            <div class="col-md-2 text-right">
                                <a href="{!! url('mediuswareadmin/team/create') !!}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add New</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="row_create_call" class="table table-striped table-hover table-bordered display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th width="5%">Avatar</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Designation</th>
                                    <th>Social Link</th>
                                    <th>Status</th>
                                    <th width="8%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="tablecontents">
                                    @foreach($teams as $key => $team)
                                        <tr class="row1" data-id="{{ $team->id }}">
                                            <td>{!! ++$key !!}</td>
                                            <td><img src="{!! asset('media/team/'.$team->avatar) !!}" width="45"></td>
                                            <td>{!! $team->name !!}</td>
                                            <td>{!! $team->email !!}</td>
                                            <td>{!! $team->phone !!}</td>
                                            <td>{!! $team->designation !!}</td>
                                            <td>
                                                @if($team->facebook_link)
                                                    <a href="{!! $team->facebook_link !!}" target="_blank" class="btn btn-facebook waves-effect btn-circle waves-light"> <i class="fab fa-facebook"></i> </a>
                                                @endif
                                                @if($team->githtb_link)
                                                    <a href="{!! $team->githtb_link !!}" target="_blank" class="btn btn-github waves-effect btn-circle waves-light"> <i class="fab fa-github"></i> </a>
                                                @endif
                                                @if($team->stackoverflow_link)
                                                    <a href="{!! $team->stackoverflow_link !!}" target="_blank" class="btn btn-googleplus waves-effect btn-circle waves-light"> <i class="fab fa-stack-overflow"></i> </a>
                                                @endif
                                                @if($team->linkedin_link)
                                                    <a href="{!! $team->linkedin_link !!}" target="_blank" class="btn btn-linkedin waves-effect btn-circle waves-light"> <i class="fab fa-linkedin"></i> </a>
                                                @endif
                                            </td>

                                            <td>
                                                @if($team->status == 'active')
                                                    <span class="badge badge-pill badge-success">Active</span>
                                                @else
                                                    <span class="badge badge-pill badge-danger">Inactive</span>
                                                @endif
                                            </td>
                                            <td>
                                                <form action="{{ route('team.destroy', $team->id)}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <a href="{!! url('mediuswareadmin/team/'.$team->id) !!}/edit" class="btn btn-info btn-circle"><i class="fa fa-edit"></i> </a>
                                                    <button type="submit" onclick="return confirm('Are you sure...?')" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i> </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('page_js')
    <script type="text/javascript">
        $(function () {

            $( "#tablecontents" ).sortable({
                items: "tr",
                cursor: 'move',
                opacity: 0.6,
                update: function() {
                    sendOrderToServer();
                }
            });

            function sendOrderToServer() {
                var order = [];

                $('tr.row1').each(function(index,element) {
                    order.push({
                        id: $(this).attr('data-id'),
                        position: index+1,
                    });
                });

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{ url('mediuswareadmin/team/sortable') }}",
                    data: {
                        order:order,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(response) {
                        if (response.status == "success") {
                            console.log(response);
                        } else {
                            console.log(response);
                        }
                    }
                });

            }
        });

    </script>

@endsection
