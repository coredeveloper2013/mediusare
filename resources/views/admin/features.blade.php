@extends('admin.master')

@section('title')
    Mediusware | Feature
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">feature</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Feature</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

    @include('admin.include.alert')

        <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-10">
                                <h4 class="card-title">All feature</h4>
                            </div>
                            <div class="col-2 text-right">
                                <a href="{!! url('mediuswareadmin/feature/create') !!}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add New</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="row_create_call" class="table table-striped table-hover table-bordered display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th width="30%">Short Description</th>
                                    <th>Status</th>
                                    <th width="7%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($features as $key => $feature)
                                <tr>
                                    <td>{!! ++$key !!}</td>
                                    <td class="text-center"> <img src="{!! asset('media/feature/'. $feature->image) !!}" width="80"></td>
                                    <td>{!! $feature->title !!}</td>
                                    <td>{!! substr($feature->short_description, 0, 60) !!}...</td>
                                    <td>
                                        @if($feature->status == 'active')
                                            <span class="badge badge-pill badge-success">Active</span>
                                        @else
                                            <span class="badge badge-pill badge-danger">Inactive</span>
                                        @endif
                                    </td>
                                    <td>
                                        <form action="{{ route('feature.destroy', $feature->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <a href="{!! url('mediuswareadmin/feature/'.$feature->id) !!}/edit" class="btn btn-info btn-circle"><i class="fa fa-edit"></i> </a>
                                            <button type="submit" onclick="return confirm('Are you sure...?')" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i> </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
