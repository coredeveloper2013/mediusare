@extends('admin.master')

@section('title')
    Mediusware | partner Create
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">partner Create</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('partner.index') !!}">partners</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10 col-sm-8">
                                <h4 class="card-title">partner Create</h4>
                            </div>
                            <div class="col-md-2 col-sm-4 text-right">
                                <a href="{!! route('partner.index') !!}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <form class="" method="post" action="{!! url('mediuswareadmin/partner') !!}" novalidate enctype="multipart/form-data">
                            @csrf
                            <div class="row">

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Name<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name" value="{!! old('name') !!}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Country<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="country" value="{!! old('country') !!}" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('country'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('country') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>About<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <textarea name="about" class="form-control{{ $errors->has('about') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">{!! old('about') !!}</textarea>
                                            @if ($errors->has('about'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('about') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Image<span class="text-danger">*</span> (300x300)</h5>
                                        <div class="controls">
                                            <input type="file" name="image" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('image'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-7">
                                    <div class="form-group validate">
                                        <h5>Status <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" name="status" value="active" required="" id="status1" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="status1">Active</label>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio"  name="status" value="deactivate" id="status" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="status">Deactivate</label>
                                                </div>
                                            </fieldset>
                                            @if ($errors->has('status'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('status') }}</strong>
                                                </span>
                                            @endif
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </div>


                            <div class="text-xs-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-inverse">Reset</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>

        <!-- First Cards Row  -->
    </div>


@endsection

@section('page_js')

@endsection
