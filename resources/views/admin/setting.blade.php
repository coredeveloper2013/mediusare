@extends('admin.master')

@section('title')
    Mediusware | Setting
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Setting</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Setting</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10 col-sm-8">
                                <h4 class="card-title">Setting</h4>
                            </div>
                            <div class="col-md-2 col-sm-4 text-right">
                                <a href="{!! url('mediuswareadmin/dashboard') !!}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <form class="" method="post" action="{!! url('mediuswareadmin/setting/store') !!}" novalidate enctype="multipart/form-data">
                            @csrf
                            <div class="row">

                                <div class="col-lg-7 col-md-7 col-xs-12" style="margin-top: 15px">
                                    <h4 class="card-title border-bottom">Company Info</h4>
                                </div>
                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Company Name<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name" value="{!! old('name', isset($setting->name) ? $setting->name:'') !!}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Company Short Name<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="short_name" value="{!! old('short_name', isset($setting->short_name) ? $setting->short_name:'') !!}" class="form-control{{ $errors->has('short_name') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('short_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('short_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Address<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <textarea  name="address" rows="5" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">{!! old('address', isset($setting->address) ? $setting->address:'') !!}</textarea>
                                            @if ($errors->has('address'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12" style="margin-top: 15px">
                                    <h4 class="card-title border-bottom">Company Contact Info</h4>
                                </div>
                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Email<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="email" name="email" value="{!! old('email', isset($setting->email) ? $setting->email:'') !!}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Phone 1<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="phone" value="{!! old('phone', isset($setting->phone) ? $setting->phone:'') !!}" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('phone'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Phone 2<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="phone_2" value="{!! old('phone_2', isset($setting->phone_2) ? $setting->phone_2:'') !!}" class="form-control{{ $errors->has('phone_2') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('phone_2'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('phone_2') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Phone 3<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="phone_3" value="{!! old('phone_3', isset($setting->phone_3) ? $setting->phone_3:'') !!}" class="form-control{{ $errors->has('phone_3') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('phone_3'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('phone_3') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Skype<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="skype" value="{!! old('skype', isset($setting->skype) ? $setting->skype:'') !!}" class="form-control{{ $errors->has('skype') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('skype'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('skype') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Map Url<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="map_url" value="{!! old('map_url', isset($setting->map_url) ? $setting->map_url:'') !!}" class="form-control{{ $errors->has('map_url') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('map_url'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('map_url') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12" style="margin-top: 15px">
                                    <h4 class="card-title border-bottom">Company Home Info</h4>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Home Title</h5>
                                        <div class="controls">
                                            <input type="text" name="home_title" value="{!! old('home_title', isset($setting->home_title) ? $setting->home_title:'') !!}" required data-validation-required-message="This field is required" class="form-control{{ $errors->has('home_title') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('home_title'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('home_title') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Home Details</h5>
                                        <div class="controls">
                                            <textarea name="home_details" class="form-control{{ $errors->has('home_details') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">{!! old('home_details', isset($setting->home_details) ? $setting->home_details:'') !!}</textarea>
                                            @if ($errors->has('home_details'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('home_details') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12" style="margin-top: 15px">
                                    <h4 class="card-title border-bottom">Company Social Media Info </h4>
                                </div>
                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Facebook Link</h5>
                                        <div class="controls">
                                            <input type="url" name="facebook_link" value="{!! old('facebook_link', isset($setting->facebook_link) ? $setting->facebook_link:'') !!}" class="form-control{{ $errors->has('facebook_link') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('facebook_link'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('facebook_link') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Twitter Link</h5>
                                        <div class="controls">
                                            <input type="url" name="twitter_link" value="{!! old('twitter_link', isset($setting->twitter_link) ? $setting->twitter_link:'') !!}" class="form-control{{ $errors->has('twitter_link') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('twitter_link'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('twitter_link') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Githtb Link</h5>
                                        <div class="controls">
                                            <input type="url" name="githtb_link" value="{!! old('githtb_link', isset($setting->githtb_link) ? $setting->githtb_link:'') !!}" class="form-control{{ $errors->has('githtb_link') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('githtb_link'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('githtb_link') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Stackoverflow Link</h5>
                                        <div class="controls">
                                            <input type="url" name="stackoverflow_link" value="{!! old('stackoverflow_link', isset($setting->stackoverflow_link) ? $setting->stackoverflow_link:'') !!}" class="form-control{{ $errors->has('stackoverflow_link') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('stackoverflow_link'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('stackoverflow_link') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Linkedin Link</h5>
                                        <div class="controls">
                                            <input type="url" name="linkedin_link" value="{!! old('linkedin_link', isset($setting->linkedin_link) ? $setting->linkedin_link:'') !!}" class="form-control{{ $errors->has('linkedin_link') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('linkedin_link'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('linkedin_link') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Pinterest Link</h5>
                                        <div class="controls">
                                            <input type="url" name="pinterest_link" value="{!! old('pinterest_link', isset($setting->pinterest_link) ? $setting->pinterest_link:'') !!}" class="form-control{{ $errors->has('pinterest_link') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('pinterest_link'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('pinterest_link') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Yutube Link</h5>
                                        <div class="controls">
                                            <input type="url" name="youtube_link" value="{!! old('youtube_link', isset($setting->youtube_link) ? $setting->youtube_link:'') !!}" class="form-control{{ $errors->has('youtube_link') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('youtube_link'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('youtube_link') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Instagram Link</h5>
                                        <div class="controls">
                                            <input type="url" name="instagram_link" value="{!! old('instagram_link', isset($setting->instagram_link) ? $setting->instagram_link:'') !!}" class="form-control{{ $errors->has('instagram_link') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('instagram_link'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('instagram_link') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="text-xs-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-inverse">Reset</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!-- First Cards Row  -->
    </div>


@endsection

@section('page_js')

@endsection
