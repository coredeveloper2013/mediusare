<div class="row">
    <div class="col-lg-5 col-md-5 col-xs-12">

        <div class="form-group">
            <h5>Title<span class="text-danger">*</span></h5>
            <div class="controls">
                <input type="text" name="title" value="{!! old('title', isset($career) ? $career->title:'') !!}" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <h5>Vacancy<span class="text-danger">*</span></h5>
            <div class="controls">
                <input type="number" name="vacancy" value="{!! old('vacancy', isset($career) ? $career->vacancy:'') !!}" class="form-control{{ $errors->has('vacancy') ? ' is-invalid' : '' }}" >
                @if ($errors->has('vacancy'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('vacancy') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <h5>Employment Status<span class="text-danger">*</span></h5>
            <div class="controls">
                <select name="employment_status" class="form-control{{ $errors->has('employment_status') ? ' is-invalid' : '' }}">
                    <option value="Full Time" {{ isset($career) && $career->employment_status == 'Full Time' ? 'selected':'' }}>Full Time</option>
                    <option value="Part Time" {{ isset($career) && $career->employment_status == 'Part Time' ? 'selected':'' }}>Part Time</option>
                </select>
                @if ($errors->has('employment_status'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('employment_status') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <h5>Job Context<span class="text-danger">*</span></h5>
            <div class="controls">
                <textarea name="job_context"  class="form-control{{ $errors->has('job_context') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">{!! old('job_context', isset($career) ? $career->job_context:'') !!}</textarea>
                @if ($errors->has('job_context'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('job_context') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <h5>Experience Requirements<span class="text-danger">*</span></h5>
            <div class="controls">
                <textarea name="experience_requirements" class="form-control{{ $errors->has('experience_requirements') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">{!! old('experience_requirements', isset($career) ? $career->experience_requirements:'') !!}</textarea>
                @if ($errors->has('experience_requirements'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('experience_requirements') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <h5>Educational Requirements<span class="text-danger">*</span></h5>
            <div class="controls">
                <textarea rows="30" name="educational_requirements"  class="form-control{{ $errors->has('educational_requirements') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">{{ old('job_responsibilities', isset($career) ? $career->educational_requirements:'') }}</textarea>
                @if ($errors->has('educational_requirements'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('educational_requirements') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <h5>Salary<span class="text-danger">*</span></h5>
            <div class="controls">
                <input type="text"  name="salary" value="{!! old('salary', isset($career) ? $career->salary:'') !!}" class="form-control{{ $errors->has('salary') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                @if ($errors->has('salary'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('salary') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <h5>Deadline<span class="text-danger">*</span></h5>
            <div class="controls">
                <input type="text" id="datepicker-autoclose"  name="deadline" value="{!! old('deadline', isset($career) ? $career->deadline:'') !!}" class="form-control{{ $errors->has('deadline') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                @if ($errors->has('deadline'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('deadline') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <h5>Image</h5>
            <div class="controls">
                <input type="file" class="form-control"  name="image" accept="image/*">
                @if(!empty($career->image))
                    <img src="{{ asset('media/career/'.$career->image) }}" width="100">
                @endif
                @if ($errors->has('image'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('image') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <h5>Apply For Email<span class="text-danger">*</span></h5>
            <div class="controls">
                <input type="email"  name="apply_email" value="{!! old('apply_email', isset($career) ? $career->apply_email:'') !!}" class="form-control{{ $errors->has('apply_email') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                @if ($errors->has('apply_email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('apply_email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group validate">
            <h5>Status <span class="text-danger">*</span></h5>
            <div class="controls">
                <fieldset>
                    <div class="custom-control custom-radio">
                        <input type="radio" name="status" value="active" {{ isset($career) && $career->status == 'active' ? 'checked':'' }} required="" id="status1" class="custom-control-input" aria-invalid="false">
                        <label class="custom-control-label" for="status1">Active</label>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="custom-control custom-radio">
                        <input type="radio"  name="status" value="inactive" id="status" {{ isset($career) && $career->status == 'inactive' ? 'checked':'' }} class="custom-control-input" aria-invalid="false">
                        <label class="custom-control-label" for="status">Inactive</label>
                    </div>
                </fieldset>
                @if ($errors->has('status'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                @endif
                <div class="help-block"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-7 col-md-7 col-xs-12">
        <div class="form-group">
            <h5>Job Responsibilities<span class="text-danger">*</span></h5>
            <div class="controls">
                <textarea rows="30" name="job_responsibilities"  class="form-control{{ $errors->has('job_responsibilities') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">{{ old('job_responsibilities', isset($career) ? $career->job_responsibilities:'') }}</textarea>
                @if ($errors->has('job_responsibilities'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('job_responsibilities') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <h5>Additional Requirements<span class="text-danger">*</span></h5>
            <div class="controls">
                <textarea rows="30"  name="additional_requirements"  class="form-control{{ $errors->has('additional_requirements') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">{{ old('additional_requirements', isset($career) ? $career->additional_requirements:'') }}</textarea>
                @if ($errors->has('additional_requirements'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('additional_requirements') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <h5>Compensation & Other Benefits</h5>
            <div class="controls">
                <textarea rows="30"  name="other_benefits"  class="form-control{{ $errors->has('other_benefits') ? ' is-invalid' : '' }} summernote" >{{ old('other_benefits', isset($career) ? $career->other_benefits:'') }}</textarea>
                @if ($errors->has('other_benefits'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('other_benefits') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <h5>Read Before Apply</h5>
            <div class="controls">
                <textarea rows="10"  name="read_before_apply"  class="form-control{{ $errors->has('read_before_apply') ? ' is-invalid' : '' }} summernote" >{{ old('read_before_apply', isset($career) ? $career->read_before_apply:'') }}</textarea>
                @if ($errors->has('read_before_apply'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('read_before_apply') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <hr>
    </div>
</div>
