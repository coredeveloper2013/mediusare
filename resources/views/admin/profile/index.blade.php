@extends('admin.master')

@section('title')
    Mediusware | Update Profile | {!! $user->name !!}
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">User Edit | {!! $user->name !!}</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{!! url('/mediuswareadmin/user') !!}">Users</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- End Bread crumb and right sidebar toggle -->

    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <div class="col-6">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-10">
                                <h4 class="card-title">Profile Update</h4>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <form class="" method="post" action="{!! route('profile.update') !!}" novalidate
                              enctype="multipart/form-data">
                            @method('post')
                            @csrf
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <h5>Name<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name" value="{!! old('name', $user->name) !!}"
                                                   class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                   required data-validation-required-message="This field is required">
                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <h5>Phone</h5>
                                        <div class="controls">
                                            <input type="text" name="phone" value="{!! old('phone', $user->phone) !!}"
                                                   class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('phone'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <h5>About</h5>
                                        <div class="controls">
                                            <textarea rows="30"  name="about"  class="form-control{{ $errors->has('about') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">{{ old('about', isset($user) ? $user->about:'') }}</textarea>
                                            @if ($errors->has('about'))
                                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('about') }}</strong>
                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <h5>Email</h5>
                                        <div class="controls">
                                            <input type="email" name="email" value="{!! old('email', $user->email) !!}"
                                                   disabled
                                                   class="form-control">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Image (300x300)</h5>
                                        <div class="controls">
                                            <input type="file" id="inputImage" name="image"
                                                   class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}">
                                            <br>
                                            <img id="previewImage" src="{!! asset('media/user/'. $user->image) !!}"
                                                 width="100">
                                            @if ($errors->has('image'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-xs-right">
                                <button type="submit" class="btn btn-info">Update</button>
                                <button type="reset" class="btn btn-inverse">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-10">
                                <h4 class="card-title">Password Update</h4>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>
                        <form class="" method="post" action="{!! route('updatePassword') !!}" novalidate
                              enctype="multipart/form-data">
                            @method('post')
                            @csrf
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <h5>Current Password<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="password" name="current_password"
                                                   class="form-control{{ $errors->has('current_password') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('current_password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('current_password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <h5>New Password<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="password" name="password" value="{!! old('password') !!}"
                                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <h5>New Confirm Password<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="password" name="password_confirmation"
                                                   value="{!! old('password_confirmation') !!}"
                                                   class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-xs-right">
                                <button type="submit" class="btn btn-info">Update</button>
                                <button type="reset" class="btn btn-inverse">Reset</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>

        <!-- First Cards Row  -->
    </div>


@endsection
@section('page_js')
@endsection
