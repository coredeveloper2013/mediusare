@if (session('success'))
    <div class="alert alert-success"> <i class="ti-check"></i>
        <b> {{ session('success') }}</b>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
@elseif(session('error'))
    <div class="alert alert-danger"> <i class="ti-close"></i>
        <b> {{ session('error') }}</b>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
@endif