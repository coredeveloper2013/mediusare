@extends('admin.master')

@section('title')
    Mediusware | Team Edit | {!! $team->name !!}
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Team Edit | {!! $team->name !!}</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('team.index') !!}">Teams</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    
    
    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-10">
                                <h4 class="card-title">Team Update</h4>
                            </div>
                            <div class="col-2 text-right">
                                <a href="{!! route('team.index') !!}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <form class="" method="post" action="{!! route('team.update', $team->id) !!}" novalidate enctype="multipart/form-data">
                            @method('PATCH')
                            @csrf
                            <div class="row">

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Name<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name" value="{!! old('name', $team->name) !!}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Designation<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="designation" value="{!! old('designation', $team->designation) !!}" class="form-control{{ $errors->has('designation') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('designation'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('designation') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Email<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="email" name="email" value="{!! old('email', $team->email) !!}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Phone<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="phone" value="{!! old('phone', $team->phone) !!}" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('phone'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <h5>About</h5>
                                        <div class="controls">
                                            <textarea  name="about"  class="form-control{{ $errors->has('about') ? ' is-invalid' : '' }} summernote" >{!! old('about', $team->about) !!}</textarea>
                                            @if ($errors->has('about'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('about') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Tag Line<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="tag_line" value="{!! old('tag_line', $team->tag_line) !!}" class="form-control{{ $errors->has('tag_line') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('tag_line'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('tag_line') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Important Skills<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="important_skills" value="{!! old('important_skills', $team->important_skills) !!}" class="form-control{{ $errors->has('important_skills') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('important_skills'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('important_skills') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Facebook Link</h5>
                                        <div class="controls">
                                            <input type="url" name="facebook_link" value="{!! old('facebook_link', $team->facebook_link) !!}" class="form-control{{ $errors->has('url') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('facebook_link'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('facebook_link') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Githtb Link</h5>
                                        <div class="controls">
                                            <input type="url" name="githtb_link" value="{!! old('githtb_link', $team->githtb_link) !!}" class="form-control{{ $errors->has('githtb_link') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('githtb_link'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('githtb_link') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Stackoverflow Link</h5>
                                        <div class="controls">
                                            <input type="url" name="stackoverflow_link" value="{!! old('stackoverflow_link', $team->stackoverflow_link) !!}" class="form-control{{ $errors->has('stackoverflow_link') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('stackoverflow_link'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('stackoverflow_link') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Linkedin Link</h5>
                                        <div class="controls">
                                            <input type="url" name="linkedin_link" value="{!! old('linkedin_link', $team->linkedin_link) !!}" class="form-control{{ $errors->has('linkedin_link') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('linkedin_link'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('linkedin_link') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Avatar (300x300)</h5>
                                        <div class="controls">
                                            <input type="file" name="avatar" class="form-control{{ $errors->has('avatar') ? ' is-invalid' : '' }}">
                                            <br>
                                            <img src="{!! asset('media/team/'. $team->avatar) !!}" width="80">
                                            @if ($errors->has('avatar'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('avatar') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group validate">
                                        <h5>Status <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" name="status" {!! $team->status == 'active' ? 'checked':'' !!} value="active" required="" id="status1" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="status1">Active</label>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio"  name="status" {!! $team->status == 'inactive' ? 'checked':'' !!} value="inactive" id="status" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="status">Inactive</label>
                                                </div>
                                            </fieldset>
                                            @if ($errors->has('status'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('status') }}</strong>
                                                </span>
                                            @endif
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                                <div class="col-lg-5 col-md-5 col-xs-12">
                                    <div class="row mb-3">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <h5>Skills Name<span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="text" name="skills_name[]" value="{!! old('skills_name') !!}" placeholder="Skills Name: HTML" class="form-control{{ $errors->has('skills_name') ? ' is-invalid' : '' }}">
                                                    @if ($errors->has('skills_name'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('skills_name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <h5>Skills Percentage<span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="number" min="10" name="skills_percentage[]" value="{!! old('skills_percentage') !!}" placeholder="Skills Percentage: 85" class="form-control{{ $errors->has('skills_percentage') ? ' is-invalid' : '' }}">
                                                    @if ($errors->has('skills_percentage'))
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('skills_percentage') }}</strong>
                                                </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <br>
                                            <div class="form-group">
                                                <button class="btn btn-success" type="button" onclick="education_fields();"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    @if(!empty($team->skills))
                                        @php $i = 1 @endphp
                                        @foreach($team->skills as $key => $skill)
                                        <div class="form-group removeclass{{$i}}">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <input type="text" name="skills_name[]" value="{!! $skill->skills_name !!}" placeholder="Skills Name: HTML" class="form-control" required="" data-validation-required-message="This field is required">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <input type="number" min="10" name="skills_percentage[]" value="{!! $skill->skills_percentage !!}" placeholder="Skills Percentage: 85" class="form-control" required="" data-validation-required-message="This field is required">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <button class="btn btn-danger" type="button" onclick="remove_education_fields({!! $i !!});"><i class="fa fa-minus"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @php $i++ @endphp
                                        @endforeach
                                    @endif

                                    <div id="education_fields"></div>
                                </div>
                            </div>


                            <div class="text-xs-right">
                                <button type="submit" class="btn btn-info">Update</button>
                                <button type="reset" class="btn btn-inverse">Reset</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
        
        <!-- First Cards Row  -->
    </div>


@endsection
@section('page_js')

@endsection
