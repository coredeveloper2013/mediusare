@extends('admin.master')

@section('title')
    Mediusware | Portfolio Create
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">portfolio Create</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('portfolio.index') !!}">Portfolio</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10 col-sm-8">
                                <h4 class="card-title">Portfolio Create</h4>
                            </div>
                            <div class="col-md-2 col-sm-4 text-right">
                                <a href="{!! route('portfolio.index') !!}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>
                        <form method="post" action="{!! url('mediuswareadmin/portfolio') !!}" novalidate enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-7 col-md-7 col-xs-12">

                                    <div class="form-group">
                                        <h5>Select Category<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <select class="select2 form-control{{ $errors->has('category') ? ' is-invalid' : '' }}"  multiple="multiple" style="height: 36px;width: 100%;" name="category[]" required data-validation-required-message="This field is required">
                                                @foreach($categories as $category)
                                                    <option value="{!! $category->id !!}">{!! $category->title !!}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('category'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('category') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Select Technology<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <select name="technology[]" class="select2 form-control{{ $errors->has('technology') ? ' is-invalid' : '' }}" multiple="multiple" style="height: 36px;width: 100%;"  required data-validation-required-message="This field is required">
                                                @foreach($technologies as $technology)
                                                    <option value="{!! $technology->id !!}">{!! $technology->title !!}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('title'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('title') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Title<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="title" value="{!! old('title') !!}" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('title'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('title') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Service Provided<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="service_provided" value="{!! old('service_provided') !!}" class="form-control{{ $errors->has('service_provided') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('service_provided'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('service_provided') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <h5>Project About<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <textarea  name="company_about"  class="form-control{{ $errors->has('company_about') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">{!! old('company_about') !!}</textarea>
                                            @if ($errors->has('company_about'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('company_about') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

{{--                                    <div class="form-group">--}}
{{--                                        <h5>Solution<span class="text-danger">*</span></h5>--}}
{{--                                        <div class="controls">--}}
{{--                                            <textarea  name="solution"  class="form-control{{ $errors->has('solution') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">{!! old('solution') !!}</textarea>--}}
{{--                                            @if ($errors->has('solution'))--}}
{{--                                                <span class="invalid-feedback" role="alert">--}}
{{--                                                    <strong>{{ $errors->first('solution') }}</strong>--}}
{{--                                                </span>--}}
{{--                                            @endif--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group">--}}
{{--                                        <h5>Impact<span class="text-danger">*</span></h5>--}}
{{--                                        <div class="controls">--}}
{{--                                            <textarea  name="impact"  class="form-control{{ $errors->has('impact') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">{!! old('impact') !!}</textarea>--}}
{{--                                            @if ($errors->has('impact'))--}}
{{--                                                <span class="invalid-feedback" role="alert">--}}
{{--                                                    <strong>{{ $errors->first('impact') }}</strong>--}}
{{--                                                </span>--}}
{{--                                            @endif--}}
{{--                                        </div>--}}
{{--                                    </div>--}}


                                    <div class="form-group">
                                        <h5>Client Name<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="client_name" value="{!! old('client_name') !!}" class="form-control{{ $errors->has('client_name') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('client_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('client_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <h5>Portfolio Url<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="url" name="url" value="{!! old('url') !!}" class="form-control{{ $errors->has('url') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('url'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('url') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Project Youtube ID</h5>
                                        <div class="controls">
                                            <input type="text" name="youtube_id" value="{!! old('youtube_id') !!}" class="form-control{{ $errors->has('youtube_id') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('youtube_id'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('youtube_id') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Portfolio Cover Image (1903x600)<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="file" name="image" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required" accept="image/*">
                                            @if ($errors->has('image'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group validate">
                                        <h5>Status <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" name="status" value="active" required="" id="status1" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="status1">Active</label>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio"  name="status" value="inactive" id="status" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="status">Inactive</label>
                                                </div>
                                            </fieldset>
                                            @if ($errors->has('status'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('status') }}</strong>
                                                </span>
                                            @endif
                                            <div class="help-block"></div>
                                        </div>
                                    </div>

                                    <div class="form-group validate">
                                        <h5>Showcase Portfolio<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" name="showcase" value="1" required="" id="showcase1" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="showcase1">Active</label>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio"  name="showcase" value="0" id="showcase" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="showcase">Inactive</label>
                                                </div>
                                            </fieldset>
                                            @if ($errors->has('status'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('status') }}</strong>
                                                </span>
                                            @endif
                                            <div class="help-block"></div>
                                        </div>
                                    </div>

                                </div>


                                <div class="col-lg-5 col-md-5 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <h5>Portfolio Feature<span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="text" name="portfolio_feature[]" value="{!! old('portfolio_feature') !!}" placeholder="" class="form-control{{ $errors->has('portfolio_feature') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                                    @if ($errors->has('portfolio_feature'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('portfolio_feature') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <br>
                                            <div class="form-group">
                                                <button class="btn btn-success" type="button" onclick="feature_added();"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="education_fields"></div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <h5>Portfolio Image<span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="file" name="portfolio_image[]" multiple>
                                                    @if ($errors->has('portfolio_image'))
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('portfolio_image') }}</strong>
                                                </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <br>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <hr>
                                    <div class="text-xs-right">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                        <button type="reset" class="btn btn-inverse">Reset</button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- First Cards Row  -->
    </div>


@endsection

@section('page_js')
    <script src="{!! asset('admin') !!}/libs/dropzone/dist/min/dropzone.min.js"></script>

    <script>
        var inc = 1;

        function feature_added() {
            inc++;
            var objTo = document.getElementById('education_fields')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "form-group removeclass" + inc);
            var rdiv = 'removeclass' + inc;
            divtest.innerHTML =
                '<div class="row">'+
                '<div class="col-md-10">'+
                '<div class="form-group">'+
                '<div class="controls">'+
                '<input type="text" name="portfolio_feature[]" value="" placeholder="" class="form-control" required data-validation-required-message="This field is required">'+

                '</div>'+
                '</div>'+
                '</div>'+

                '<div class="col-sm-2"> ' +
                '<div class="form-group"> ' +
                '<button class="btn btn-danger" type="button" onclick="remove_feature(' + inc + ');"> <i class="fa fa-minus"></i> </button> ' +
                '</div>' +
                '</div>'+
                '</div>';

            objTo.appendChild(divtest)
        }

        function remove_feature(rid) {
            $('.removeclass' + rid).remove();
        }
    </script>

    <script>
        $(document).ready(function() {
            $('input[name="portfolio_image[]"]').fileuploader({
                addMore: true,
                files: null,
            });
        });

    </script>
@endsection
