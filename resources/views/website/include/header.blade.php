<header id="{!! Request::is('home') == 'home' ? 'header2':'header' !!}">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-between">
            <div id="logo">
                <a href="{!! url('/home') !!}">
                    <img class="logo-1" src="{!! asset('/assets') !!}/image/logo.jpg" alt="" style="width: 80px" title="" />
                </a>
                <a href="{!! url('/home') !!}">
                    <img class="logo-2" src="{!! asset('/assets') !!}/image/logo.jpg" alt="" style="width: 80px" title="" />
                </a>
            </div>
            <div class="nav-wrap d-flex flex-row align-items-center">
                <nav id="nav-menu-container">
                    <ul class="nav-menu nav-menu2">
                        <li >
                            <a class="{!! Request::is('home') == 'home' ? 'menu-active':'' !!}" href="{!! url('/home') !!}">Home</a>
                        </li>
                        <li>
                            <a class="{!! Request::is('features') == 'features' ? 'menu-active':'' !!}" href="{!! url('/features') !!}">Features</a>
                        </li>
                        <li>
                            <a class="{!! Request::is('teams') == 'teams' ? 'menu-active':'' !!}" href="{!! url('/teams') !!}">Teams</a>
                        </li>
                        <li>
                            <a class="{!! Request::is('portfolios') == 'portfolios' ? 'menu-active':'' !!}" href="{!! url('/portfolios') !!}">Portfolios</a>
                        </li>
                        <li>
                            <a class="{!! Request::is('services') == 'services' ? 'menu-active':'' !!}" href="{!! url('/services') !!}">Services</a>
                        </li>
                        <li>
                            <a class="{!! Request::is('pricing') == 'pricing' ? 'menu-active':'' !!}" href="{!! url('/pricing') !!}">Pricing</a>
                        </li>
                        <li>
                            <a class="{!! Request::is('about') == 'about' ? 'menu-active':'' !!}" href="{!! url('/about') !!}">About</a>
                        </li>

{{--                        <li class="menu-has-children">--}}
{{--                            <a href="#">Blog</a>--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--                                    <a href="blog-home.html">Blog Home</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="blog-home-sidebar.html">Blog Home Sidebar</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="blog-list.html">Blog List</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="blog-list-sidebar.html">Blog List Sidebar</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="blog-details.html">Blog Details</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="blog-details-sidebar.html">Blog Details Sidebar</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
                        <li>
                            <a class="{!! Request::is('contact') == 'contact' ? 'menu-active':'' !!}" href="{!! url('/contact') !!}">Contact</a>
                        </li>
                    </ul>
                </nav>

            </div>
            <!-- #nav-menu-container -->
        </div>
    </div>
</header>