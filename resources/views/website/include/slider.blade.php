<section class="hero-section-bg relative">
    <div id="app-owl" class="owl-carousel owl-theme">
        <div class="items">
            <img class="owl-img" src="{!! asset('/assets') !!}/img/carusel/s1.jpg" alt="">
        </div>
        <div class="items">
            <img class="owl-img" src="{!! asset('/assets') !!}/img/carusel/s2.jpg" alt="">
        </div>
        <div class="items">
            <img class="owl-img" src="{!! asset('/assets') !!}/img/carusel/s3.jpg" alt="">
        </div>
    </div>
    <div class="content">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-between">
                <div class="offset-lg-1 col-lg-5 offset-md-0 col-md-10">
                    <h1 class="text-white">
                        We Made our <br>
                        Software 100% Errorless
                    </h1>
                    <p class="pt-20 pb-20 mw-510 text-white">
                        The first is a non technical method which requires the use of adware removal software.
                        Download free adware and spyware removal software and use advanced tools to help prevent
                        getting infected.
                    </p>
                    <a href="#" class="genric-btn">Browse free demo</a>
                </div>
                <div class="col-lg-6">
                    <div class="hero-img8">
                        <img class="" src="{!! asset('/assets') !!}/img/mockup.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>