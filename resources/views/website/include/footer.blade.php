<footer class="footer-area section-gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="single-footer-widget">
                    <h4>About Us</h4>
                    <p>
                        About 64% of all on-line teens say that do things online that they wouldn’t want their
                        parents to know about. 11% of all
                        adult internet user visit websites.
                    </p>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="single-footer-widget">
                    <h4>Resources</h4>
                    <ul class="menu-list">
                        <li>
                            <a href="#">Guides</a>
                        </li>
                        <li>
                            <a href="#">Research</a>
                        </li>
                        <li>
                            <a href="#">Experts</a>
                        </li>
                        <li>
                            <a href="#">Agencies</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="single-footer-widget">
                    <h4>Features</h4>
                    <ul class="menu-list">
                        <li>
                            <a href="#">Jobs</a>
                        </li>
                        <li>
                            <a href="#">Brand Assets</a>
                        </li>
                        <li>
                            <a href="#">Investor Relations</a>
                        </li>
                        <li>
                            <a href="terms-condition.html">Terms of Service</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="single-footer-widget">
                    <h4>Follow Us</h4>
                    <ul class="social-icons">
                        <li>
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-dribbble"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-behance"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-footer-widget">
                    <h4>Newsletter</h4>
                    <p>You can trust us. we only send promo offers, not a single spam.</p>
                    <div class="d-flex flex-row newsletter-form" id="mc_embed_signup">
                        <form class="navbar-form" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                              method="get">
                            <div class="d-flex form-wrap">
                                <input class="form-control" name="EMAIL" autocomplete="email" placeholder="Your email address"
                                       onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email address'"
                                       required="" type="email">
                                <button class="submit-btn">
                                    <span class="lnr lnr-arrow-right"></span>
                                </button>
                            </div>
                            <div class="info mt-20"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom row justify-content-center mt-70">
            <p class="footer-text m-0 col-lg-6 col-md-12">Copyright © 2019 All rights reserved to
                <a href="https://themeforest.net/user/codethemes/portfolio">Code Themes</a>
            </p>
        </div>
    </div>

</footer>