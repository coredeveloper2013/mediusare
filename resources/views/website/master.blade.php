<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{!! asset('/assets') !!}/img/favicon.png">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Mediusware</title>
    <meta charset="UTF-8">
    <meta name="author" content="Mediusware">
    <meta name="description" content="Have a look, what we done for our clients!">
    <meta name="keywords" content="mediusware,portfolio,web development, laravel,html,css,vuejs">

    <meta property="og:title" content="Mediusware"/>
    <meta property="og:url" content="https://mediusware.com"/>
    <meta property="og:description" content="Have a look, what we done for our clients!"/>
    <meta property="og:image" content="{{ asset('assets/img/share-banner.png') }}"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="x-ua-compatible" content="IE=edge">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="{!! asset('/assets') !!}/css/linearicons.css">
    <link rel="stylesheet" href="{!! asset('/assets') !!}/css/font-awesome.min.css">
    <link rel="stylesheet" href="{!! asset('/assets') !!}/css/bootstrap.css">
    <link rel="stylesheet" href="{!! asset('/assets') !!}/css/magnific-popup.css">
    <link rel="stylesheet" href="{!! asset('/assets') !!}/css/nice-select.css">
    <link rel="stylesheet" href="{!! asset('/assets') !!}/css/animate.min.css">
    <link rel="stylesheet" href="{!! asset('/assets') !!}/css/owl.carousel.css">
    <link rel="stylesheet" href="{!! asset('/assets') !!}/css/animate.css">
    <link rel="stylesheet" href="{!! asset('/assets') !!}/css/main.css">
</head>

<body class="version8">

    <!-- Start main content -->
        @yield('content')
    <!-- End main content -->

    <link rel="stylesheet" href="{!! asset('/assets') !!}/fbmsg.css">

    <script src="{!! asset('/assets') !!}/js/vendor/jquery-2.2.4.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <script src="{!! asset('/assets') !!}/fbmsg.js"></script>
    <script src="{!! asset('/assets') !!}/js/popper.min.js"></script>
    <script src="{!! asset('/assets') !!}/js/vendor/bootstrap.min.js"></script>
    <script src="{!! asset('/assets') !!}/js/vendor/jquery.vide.min.js"></script>
    <script src="{!! asset('/assets') !!}/js/easing.min.js"></script>
    <script src="{!! asset('/assets') !!}/js/superfish.min.js"></script>
    <script src="{!! asset('/assets') !!}/js/jquery.ajaxchimp.min.js"></script>
    <script src="{!! asset('/assets') !!}/js/owl.carousel.min.js"></script>
    <script src="{!! asset('/assets') !!}/js/mail-script.js"></script>
    <script src="{!! asset('/assets') !!}/js/wow.js"></script>
    <script src="{!! asset('/assets') !!}/js/youtube.min.js"></script>
    <script>
        // $(document).ready(function() {
        //     $(this).scrollTop(0);
        // });

        $.fn.andSelf = function() {
            return this.addBack.apply(this, arguments);
        }


    </script>
    @yield('page_script')
</body>
</html>
