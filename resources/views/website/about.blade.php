@extends('website.master')

@section('title')
    Mediusware | About
@endsection

@section('content')
    <!-- Start page-top section -->
    <section class="page-top-section">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6 col-md-6">
                    <h1 class="text-white">About Us</h1>
                </div>
                <div class="col-lg-6  col-md-6 page-top-nav">
                    <div>
                        <a href="{!! url('/home') !!}">Home</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="about.html">About Us</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End page-top section -->

    <!-- Start about section -->
    <section class="about-section section-gap">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-6 about-left">
                    <h1>
                        Our Mission
                    </h1>
                    <p class="pt-20 pb-20 fadeInUp">
                        To enhancing the business growth of our customers with creative Design and Development to deliver market-defining high-quality solutions that create value and consistent competitive advantage for our clients around the world.
                    </p>
                    <a href="#" class="genric-btn gradient-bg2 fadeInUp">Browse free demo</a>
                </div>
                <div class="col-lg-6 text-center">
                    <img class="img-fluid" src="{!! asset('/assets') !!}/img/about-img.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- End about section -->

    <section class="marketing-section section-gap aquablue-bg">

        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-6 text-center">
                    <img class="img-fluid" src="{!! asset('/assets') !!}/img/about-img.png" alt="">
                </div>

                <div class="col-lg-6 about-left">
                    <h1>
                        Our Vision
                    </h1>
                    <p class="pt-20 pb-20 fadeInUp">
                        To become a prime performer, in providing quality Web and Software solutions in the competitive global market place.
                    </p>
                    <a href="#" class="genric-btn gradient-bg2 fadeInUp">Browse free demo</a>
                </div>

            </div>
        </div>

    </section>

    <section class="about-section section-gap">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-6 about-left">
                    <h1>
                        Our Commitment
                    </h1>
                    <p class="pt-20 pb-20 fadeInUp">
                        We take pride in our on time delivery and ability to meet quick turn around requests while exceeding customer quality demands. Customer Satisfaction continues to be of utmost importance , as do Consistent quality, Constant innovation, Technology enhancement, Process improvement and Customer orientation
                    </p>
                    <a href="#" class="genric-btn gradient-bg2 fadeInUp">Browse free demo</a>
                </div>
                <div class="col-lg-6 text-center">
                    <img class="img-fluid" src="{!! asset('/assets') !!}/img/about-img.png" alt="">
                </div>
            </div>
        </div>
    </section>


@endsection
