@extends('website.master')

@section('title')
    Mediusware | Home
@endsection

@section('content')

    <!-- Start hero-section -->
    @include('website.include.slider')
    <!-- End hero-section -->

    <!-- Start feature section -->
    <section class="feature-section pb-120 pt-90">
        <div class="container">
            <div class="row feature-left-wrapr">
                <div class="col-lg-4 col-md-6">
                    <div class="single-feature aquablue-bg">
                        <div class="icon">
                            <span>
                                <img width="39" height="40" src="{!! asset('/assets') !!}/img/feature/i1.png"
                                     class="attachment-image_icon_40x40 size-image_icon_40x40" alt=""> </span>
                        </div>
                        <div class="desc">
                            <h4>Collaboration</h4>
                            <p class="mb-0">
                                Some days a motivational quote can provide a quick pick-me-up for employees and even
                                management.

                            </p>
                        </div>
                    </div>
                </div>


                <div class="col-lg-4 col-md-6">
                    <div class="single-feature aquablue-bg">
                        <div class="icon">
                            <span>
                                <img width="40" height="40" src="{!! asset('/assets') !!}/img/feature/i3.png"
                                     class="attachment-image_icon_40x40 size-image_icon_40x40" alt=""> </span>
                        </div>
                        <div class="desc">
                            <h4>Full of features</h4>
                            <p class="mb-0">
                                Some days a motivational quote can provide a quick pick-me-up for employees and even
                                management.

                            </p>
                        </div>
                    </div>
                </div>


                <div class="col-lg-4 col-md-6">
                    <div class="single-feature aquablue-bg">
                        <div class="icon">
                            <span>
                                <img width="40" height="36" src="{!! asset('/assets') !!}/img/feature/i4.png"
                                     class="attachment-image_icon_40x40 size-image_icon_40x40" alt=""> </span>
                        </div>
                        <div class="desc">
                            <h4>Global Framework</h4>
                            <p class="mb-0">
                                Some days a motivational quote can provide a quick pick-me-up for employees and even
                                management.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End feature Area -->

    <!-- Start marketing-section -->
    <section class="marketing-section section-gap aquablue-bg">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-6 marketing-left">
                    <h1>
                        Build Your Brand
                        <br> Automate Your Marketing
                    </h1>
                    <p class="mw-510">
                        The first is a non technical method which requires the use of adware removal software. Download
                        free adware and spyware removal
                        software and use advanced tools to help prevent getting infected. Spyware scan review is a free
                        service
                        for anyone interested in downloading spyware/adware removal software. Our adware remover is the
                        most
                        trusted.
                    </p>
                    <a href="#" class="genric-btn3">Browse free demo</a>
                </div>
                <div class="col-lg-6 marketing-right">
                    <div class="active-dash-carusel">
                        <img class="img-fluid item" src="{!! asset('/assets') !!}/img/marketing/d1.png" alt="">
                        <img class="img-fluid item" src="{!! asset('/assets') !!}/img/marketing/d2.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End marketing-section -->

    <!-- Start stat-section -->
    <section class="stat-section section-gap">
        <div class="container">
            <div class="row justify-content-center section-title-wrap">
                <div class="col-lg-12">
                    <div class="title-img">
                        <img src="{!! asset('/assets') !!}/img/title-icon.png" alt="">
                    </div>
                    <h1>We glow with Our Statistics</h1>
                    <p>
                        The first is a non technical method which requires the use of adware removal software. Download
                        free adware and spyware removal
                        software and use advanced tools getting infected.
                    </p>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-4 col-md-12 text-centerpr-3 pr-md-5">
                    <img class="img-fluid" src="{!! asset('/assets') !!}/img/stat/stat-img.png" alt="">
                </div>
                <div class="col-lg-8 col-md-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="single-stat">
                                <div class="icon">
                                    <img src="{!! asset('/assets') !!}/img/stat/s1.png" alt="">
                                </div>
                                <div class="desc">
                                    <a href="#" class="d-block">
                                        <h4>Productivity</h4>
                                    </a>
                                    <p>
                                        Making changes in your life is great and it is the way we grow and develop as
                                        people. Change is a constant.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="single-stat">
                                <div class="icon">
                                    <img src="{!! asset('/assets') !!}/img/stat/s2.png" alt="">
                                </div>
                                <div class="desc">
                                    <a href="#" class="d-block">
                                        <h4>Sales Monitoring</h4>
                                    </a>
                                    <p>
                                        Making changes in your life is great and it is the way we grow and develop as
                                        people. Change is a constant.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="single-stat">
                                <div class="icon">
                                    <img src="{!! asset('/assets') !!}/img/stat/s3.png" alt="">
                                </div>
                                <div class="desc">
                                    <a href="#" class="d-block">
                                        <h4>Payment Stats</h4>
                                    </a>
                                    <p>
                                        Making changes in your life is great and it is the way we grow and develop as
                                        people. Change is a constant.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="single-stat">
                                <div class="icon">
                                    <img src="{!! asset('/assets') !!}/img/stat/s4.png" alt="">
                                </div>
                                <div class="desc">
                                    <a href="#" class="d-block">
                                        <h4>Pending Clearnance</h4>
                                    </a>
                                    <p>
                                        Making changes in your life is great and it is the way we grow and develop as
                                        people. Change is a constant.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End stat-section -->

    <!-- Start cta-section -->
    <section class="cta-section gradient-bg">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-lg-8">
                    <h1 class="text-white mb-5">Fallen in Love with <br>
                        our features? Get a free trial for 14 days!</h1>
                    <div class="cta-btn">
                        <a href="#" class="ct-btn1 mr-sm-3 mb-sm-0 mb-3">Start free trial</a>
                        <a href="#" class="ct-btn2 active">Signup</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End cta-section -->

    <!-- Start social section -->
    <section class="social-section section-gap">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 social-left">
                    <h1>
                        Manage Social Network with Our Managing Toolkit
                    </h1>
                    <p>
                        The first is a non technical method which requires the use of adware removal software. Download
                        free adware and spyware removal
                        software and use advanced tools to help prevent getting infected. Spyware scan review is a free
                        service
                        for anyone interested in downloading spyware/adware removal software. Our adware remover is the
                        most
                        trusted.
                    </p>
                    <a href="#" class="genric-btn2">Browse free demo</a>
                </div>
                <div class="col-lg-6 text-center social-right">
                    <img class="img-fluid" src="{!! asset('/assets') !!}/img/social-img.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- End social section -->

    <!-- Start price section -->
    <section class="price-section section-gap">
        <div class="container">
            <div class="row justify-content-center section-title-wrap">
                <div class="col-lg-12">
                    <div class="title-img">
                        <img src="{!! asset('/assets') !!}/img/title-icon.png" alt="">
                    </div>
                    <h1>Select your Best Pricing</h1>
                    <p>
                        The first is a non technical method which requires the use of adware removal software. Download
                        free adware and spyware removal
                        software and use advanced tools getting infected.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="single-price">
                        <div class="price-top text-center relative">
                            <div class="pack-desc relative">
                                <h4>Starter</h4>
                                <p>
                                    Small business holders
                                </p>
                            </div>
                            <div class="price relative">
                                <p>
                                    <span>£29</span>/mo</p>
                            </div>
                        </div>
                        <div class="price-bottom">
                            <ul class="packlist">
                                <li class="">
                                    <span>2 User permission</span>
                                </li>
                                <li class="">
                                    <span>Customization Allowed</span>
                                </li>
                                <li class="">
                                    <span>Social media tool Enabled</span>
                                </li>
                                <li class="">
                                    <span>No Online Support</span>
                                </li>
                                <li class="">
                                    <span>6 Months Backup</span>
                                </li>
                            </ul>
                            <a href="#" class="genric-btn2">Get Started</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="single-price">
                        <div class="price-top text-center relative">
                            <div class="pack-desc relative">
                                <h4>Standard</h4>
                                <p>
                                    Small business holders
                                </p>
                            </div>
                            <div class="price relative">
                                <p>
                                    <span>£49</span>/mo</p>
                            </div>
                        </div>
                        <div class="price-bottom">
                            <ul class="packlist">
                                <li class="">
                                    <span>2 User permission</span>
                                </li>
                                <li class="">
                                    <span>Customization Allowed</span>
                                </li>
                                <li class="">
                                    <span>Social media tool Enabled</span>
                                </li>
                                <li class="">
                                    <span>No Online Support</span>
                                </li>
                                <li class="">
                                    <span>6 Months Backup</span>
                                </li>
                            </ul>
                            <a href="#" class="genric-btn2">Get Started</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="single-price">
                        <div class="price-top text-center relative">
                            <div class="pack-desc relative">
                                <h4>Premium</h4>
                                <p>
                                    Small business holders
                                </p>
                            </div>
                            <div class="price relative">
                                <p>
                                    <span>£99</span>/mo</p>
                            </div>
                        </div>
                        <div class="price-bottom">
                            <ul class="packlist">
                                <li class="">
                                    <span>2 User permission</span>
                                </li>
                                <li class="">
                                    <span>Customization Allowed</span>
                                </li>
                                <li class="">
                                    <span>Social media tool Enabled</span>
                                </li>
                                <li class="">
                                    <span>No Online Support</span>
                                </li>
                                <li class="">
                                    <span>6 Months Backup</span>
                                </li>
                            </ul>
                            <a href="#" class="genric-btn2">Get Started</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End price section -->

    <!-- Start unique-feature Area -->
    <section class="unique-feature-area section-gap">
        <div class="container-fluid">
            <div class="row justify-content-center section-title-wrap">
                <div class="col-lg-12">
                    <div class="title-img">
                        <img src="{!! asset('/assets') !!}/img/title-icon.png" alt="">
                    </div>
                    <h1>Features that make us Stand Out </h1>
                    <p>
                        The first is a non technical method which requires the use of adware removal software. Download
                        free adware and spyware removal
                        software and use advanced tools getting infected.
                    </p>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-6 pr-5">
                    <div class="active-execution-carusel">
                        <img class="img-fluid" src="{!! asset('/assets') !!}/img/carusel/c1.png" alt="">
                        <img class="img-fluid" src="{!! asset('/assets') !!}/img/carusel/c2.png" alt="">
                        <img class="img-fluid" src="{!! asset('/assets') !!}/img/carusel/c3.png" alt="">
                    </div>
                </div>
                <div class="col-lg-6 pl-4">
                    <div class="feature-list d-flex flex-row">
                        <div class="icon">
                            <img class="img-fluid" src="{!! asset('/assets') !!}/img/feature/f1.png" alt="">
                        </div>
                        <div class="desc ml-40">
                            <a href="#">
                                <h4 class="mb-20">Certifications</h4>
                            </a>
                            <p>
                                We all live in an age that belongs to the young at heart. Life that is becoming
                                extremely fast, day to day, also asks us to remain.
                            </p>
                        </div>
                    </div>
                    <div class="feature-list d-flex flex-row">
                        <div class="icon">
                            <img class="img-fluid" src="{!! asset('/assets') !!}/img/feature/f2.png" alt="">
                        </div>
                        <div class="desc ml-40">
                            <a href="#">
                                <h4 class="mb-20">Page Builder</h4>
                            </a>
                            <p>
                                Vera sought out counseling with me because her doctor advised her to discover the
                                emotional causes of her chronic fatigue.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End unique-feature Area -->

    <!-- Start brands section -->
    <section class="brands-section pb-120 pt-70">
        <div class="container">
            <div class="row justify-content-center brand-wrap">
                <div class="col-md-2 col-3 mt-50 text-center">
                    <a href="#">
                        <img class="img-fluid" src="{!! asset('/assets') !!}/img/brands/b1.png" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-3 mt-50 item text-center">
                    <a href="#">
                        <img class="img-fluid" src="{!! asset('/assets') !!}/img/brands/b2.png" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-3 mt-50 text-center">
                    <a href="#">
                        <img class="img-fluid" src="{!! asset('/assets') !!}/img/brands/b3.png" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-3 mt-50 text-center">
                    <a href="#">
                        <img class="img-fluid" src="{!! asset('/assets') !!}/img/brands/b4.png" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-3 mt-50 text-center">
                    <a href="#">
                        <img class="img-fluid" src="{!! asset('/assets') !!}/img/brands/b5.png" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-3 mt-50 text-center">
                    <a href="#">
                        <img class="img-fluid" src="{!! asset('/assets') !!}/img/brands/b6.png" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-3 mt-50 text-center">
                    <a href="#">
                        <img class="img-fluid" src="{!! asset('/assets') !!}/img/brands/b7.png" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-3 mt-50 text-center">
                    <a href="#">
                        <img class="img-fluid" src="{!! asset('/assets') !!}/img/brands/b8.png" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-3 mt-50 text-center">
                    <a href="#">
                        <img class="img-fluid" src="{!! asset('/assets') !!}/img/brands/b9.png" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-3 mt-50 text-center">
                    <a href="#">
                        <img class="img-fluid" src="{!! asset('/assets') !!}/img/brands/b10.png" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- End brands section -->

    <!-- Start client-review section -->
    <section class="client-review-section section-gap">
        <div class="container">
            <div class="active-review-carusel">
                <div class="single-review">
                    <div class="quote-wrap">
                        <p>
                            “Who are in extremely love with eco friendly system. Lorem ipsum dolor sit amet,
                            consectetur adipisicing elit, sed do eiusmod tempor.”
                        </p>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                        </div>
                    </div>
                    <div class="userinfo-wrap aquablue-bg d-flex flex-row align-items-center relative">
                        <div class="overlay overlay-bg"></div>
                        <div class="thumb relative">
                            <img src="{!! asset('/assets') !!}/img/review/u2.png" alt="">
                        </div>
                        <div class="details relative">
                            <h4>Vera Ball</h4>
                            <p>Head of Marketing, Apple Inc.</p>
                        </div>
                    </div>
                </div>
                <div class="single-review">
                    <div class="quote-wrap">
                        <p>
                            “Who are in extremely love with eco friendly system. Lorem ipsum dolor sit amet,
                            consectetur adipisicing elit, sed do eiusmod tempor.”
                        </p>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                        </div>
                    </div>
                    <div class="userinfo-wrap aquablue-bg d-flex flex-row align-items-center relative">
                        <div class="overlay overlay-bg"></div>
                        <div class="thumb relative">
                            <img src="{!! asset('/assets') !!}/img/review/u3.png" alt="">
                        </div>
                        <div class="details relative">
                            <h4>Derek Malone</h4>
                            <p>Head of Sales, Apple Inc.</p>
                        </div>
                    </div>
                </div>
                <div class="single-review">
                    <div class="quote-wrap">
                        <p>
                            “Who are in extremely love with eco friendly system. Lorem ipsum dolor sit amet,
                            consectetur adipisicing elit, sed do eiusmod tempor.”
                        </p>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                        </div>
                    </div>
                    <div class="userinfo-wrap aquablue-bg d-flex flex-row align-items-center relative">
                        <div class="overlay overlay-bg"></div>
                        <div class="thumb relative">
                            <img src="{!! asset('/assets') !!}/img/review/u1.png" alt="">
                        </div>
                        <div class="details relative">
                            <h4>Stella Pierce</h4>
                            <p>Head of Advertise, Apple Inc.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End client-review section -->

    <!-- Start latest-blog section -->
    <section class="latest-blog-section section-gap aquablue-bg">
        <div class="container">
            <div class="row justify-content-center section-title-wrap">
                <div class="col-lg-12">
                    <div class="title-img">
                        <img src="{!! asset('/assets') !!}/img/title-icon.png" alt="">
                    </div>
                    <h1>Latest Posts from our Blog</h1>
                    <p>
                        The first is a non technical method which requires the use of adware removal software. Download
                        free adware and spyware removal
                        software and use advanced tools getting infected.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="single-card card">
                        <img class="card-top-img" src="{!! asset('/assets') !!}/img/blog/b1.jpg" alt="Card image cap">
                        <div class="card-body">
                            <p class="card-subtitle mb-3">10 April, 2018</p>
                            <a href="#">
                                <h4 class="card-title mb-4">Motivation And Your Personal Vision An Force</h4>
                            </a>
                            <a href="#" class="card-link">
                                <i class="fas fa-eye"></i>4.5k Views</a>
                            <a href="#" class="card-link">
                                <i class="fas fa-comment-dots"></i>07</a>
                            <a href="#" class="card-link">
                                <i class="fas fa-share"></i>362</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-card card">
                        <img class="card-top-img" src="{!! asset('/assets') !!}/img/blog/b2.jpg" alt="Card image cap">
                        <div class="card-body">
                            <p class="card-subtitle mb-3">10 April, 2018</p>
                            <a href="#">
                                <h4 class="card-title mb-4">Benjamin Franklin S Method Of Habit Formation</h4>
                            </a>
                            <a href="#" class="card-link">
                                <i class="fas fa-eye"></i>4.5k Views</a>
                            <a href="#" class="card-link">
                                <i class="fas fa-comment-dots"></i>07</a>
                            <a href="#" class="card-link">
                                <i class="fas fa-share"></i>362</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-card card">
                        <img class="card-top-img" src="{!! asset('/assets') !!}/img/blog/b3.jpg" alt="Card image cap">
                        <div class="card-body">
                            <p class="card-subtitle mb-3">10 April, 2018</p>
                            <a href="#">
                                <h4 class="card-title mb-4">How To Set Intentions That Energize You</h4>
                            </a>
                            <a href="#" class="card-link">
                                <i class="fas fa-eye"></i>4.5k Views</a>
                            <a href="#" class="card-link">
                                <i class="fas fa-comment-dots"></i>07</a>
                            <a href="#" class="card-link">
                                <i class="fas fa-share"></i>362</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End latest-blog section -->

    <!-- Start team section -->
    <section class="client-review-section section-gap">
        <div class="container">

            <div class="row justify-content-center section-title-wrap">
                <div class="col-lg-12">
                    <div class="title-img">
                        <img src="{!! asset('/assets') !!}/img/title-icon.png" alt="">
                    </div>
                    <h1>Our <span class="text-info">Expertise</span> </h1><br>
                    <p>Our most stylish homegrown talents on their passions</p>
                </div>
            </div>

            <div class="row blog">

                <div class="col-md-12">
                    <div id="blogCarousel" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#blogCarousel" data-slide-to="1"></li>
                        </ol>

                        <!-- Carousel items -->
                        <div class="carousel-inner">

                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <div class="our-team">
                                            <div class="pic">
                                                <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                                            </div>
                                            <div class="team-content">
                                                <h3 class="title">Dana ROBINSON</h3>
                                                <span class="post">Marketing Consultant</span>
                                            </div>
                                            <ul class="social">
                                                <li>
                                                    <a href="#" class="fa fa-envelope"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <div class="our-team">
                                            <div class="pic">
                                                <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                                            </div>
                                            <div class="team-content">
                                                <h3 class="title">Dr. Bernard COVA</h3>
                                                <span class="post">Marketing - Auteur</span>
                                            </div>
                                            <ul class="social">
                                                <li>
                                                    <a href="#" class="fa fa-envelope"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6">
                                        <div class="our-team">
                                            <div class="pic">
                                                <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                                            </div>
                                            <div class="team-content">
                                                <h3 class="title">Dr. Francis Guilbert</h3>
                                                <span class="post">Docteur - Chercheur</span>
                                            </div>
                                            <ul class="social">
                                                <li>
                                                    <a href="#" class="fa fa-envelope"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <div class="our-team">
                                            <div class="pic">
                                                <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                                            </div>
                                            <div class="team-content">
                                                <h3 class="title">Dr. Georges WANET</h3>
                                                <span class="post">Docteur</span>
                                            </div>
                                            <ul class="social">
                                                <li>
                                                    <a href="#" class="fa fa-envelope"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--.row-->
                            </div>
                            <!--.item-->

                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <div class="our-team">
                                            <div class="pic">
                                                <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                                            </div>
                                            <div class="team-content">
                                                <h3 class="title">Ted SICHELMAN</h3>
                                                <span class="post">Law expert</span>
                                            </div>
                                            <ul class="social">
                                                <li>
                                                    <a href="#" class="fa fa-envelope"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-6 col-sm-6">
                                        <div class="our-team">
                                            <div class="pic">
                                                <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                                            </div>
                                            <div class="team-content">
                                                <h3 class="title">Dr. Noël ALBERT</h3>
                                                <span class="post">Marketing Consultant</span>
                                            </div>
                                            <ul class="social">
                                                <li>
                                                    <a href="#" class="fa fa-envelope"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <div class="our-team">
                                            <div class="pic">
                                                <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                                            </div>
                                            <div class="team-content">
                                                <h3 class="title">Dr. Saverio TOMASELLA</h3>
                                                <span class="post">Auteur et Chercheur - Brand Psychoanalysis</span>
                                            </div>
                                            <ul class="social">
                                                <li>
                                                    <a href="#" class="fa fa-envelope"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <div class="our-team">
                                            <div class="pic">
                                                <img src="https://i.ibb.co/L8Pj1mg/o6EuTCT6.jpg">
                                            </div>
                                            <div class="team-content">
                                                <h3 class="title">Ray BENEDICKTUS</h3>
                                                <span class="post">Experienced Marketing Consultant</span>
                                            </div>
                                            <ul class="social">
                                                <li>
                                                    <a href="#" class="fa fa-envelope"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--.row-->
                            </div>
                            <!--.item-->

                        </div>
                        <!--.carousel-inner-->
                    </div>
                    <!--.Carousel-->

                </div>
            </div>
        </div>
    </section>
    <!-- End team section -->

@endsection