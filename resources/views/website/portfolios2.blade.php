@extends('website.master')

@section('title')
    Mediusware | Portfolios
@endsection

@section('content')
    <!-- Start page-top section -->
    <section class="page-top-section">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6 col-md-6">
                    <h1 class="text-white">Portfolio</h1>
                </div>
                <div class="col-lg-6  col-md-6 page-top-nav">
                    <div>
                        <a href="{!! url('/') !!}">Home</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="teams.html">Portfolio</a>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- End page-top section -->

    <section id="portfolio" class="bl-expand bl-expand-top">
        <div class="container">
            <div class="row justify-content-center section-title-wrap" style="padding: 50px 0">
                <div class="col-lg-12">
                    <div class="title-img">
                        <img src="{!! asset('/assets') !!}/img/title-icon.png" alt="">
                    </div>
                    <h1>Our All <span class="text-info">Portfolio</span> Here</h1><br>
                    <p>Our most stylish homegrown talents on their passions</p>
                </div>
            </div>


            <div class="text-center mb-50">
                <button class="ron-btn filter-button" data-filter="all">All</button>
                <button class="ron-btn filter-button" data-filter="hdpe">Design</button>
                <button class="ron-btn filter-button" data-filter="sprinkle">Laravel</button>
                <button class="ron-btn filter-button" data-filter="spray">Vue</button>
                <button class="ron-btn filter-button" data-filter="irrigation">WordPress</button>
            </div>


            <div class="row mb-30">

                <div class="col-xs-12">
                    <ul class="portfolio_items" id="portfolio_items">
                        <li data-panel="panel-1" class="portfolio filter hdpe">
                            <a href="{!! url('portfolio/details') !!}">
                                <div class="post_thumb">
                                    <img src="{!! asset('/assets') !!}/img/blog/b1.jpg" alt="">
                                    <div class="overlay">
                                        <i class="fa fa-link"></i>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li  class="portfolio filter sprinkle">
                            <a href="{!! url('portfolio/details') !!}">
                            <div class="post_thumb">
                                <img src="{!! asset('/assets') !!}/img/blog/b1.jpg" alt="">
                                <div class="overlay">
                                    <i>Title: Project Title</i><br>
                                    <i class="fa fa-link"></i>
                                </div>
                            </div>
                            </a>
                        </li>

                        <li  class="portfolio filter sprinkle">
                            <a href="{!! url('portfolio/details') !!}">
                            <div class="post_thumb">
                                <img src="{!! asset('/assets') !!}/img/blog/b1.jpg" alt="">
                                <div class="overlay">
                                    <i class="fa fa-link"></i>
                                </div>
                            </div>
                            </a>
                        </li>

                        <li  class="portfolio filter hdpe">
                            <a href="{!! url('portfolio/details') !!}">
                            <div class="post_thumb">
                                <img src="{!! asset('/assets') !!}/img/blog/b1.jpg" alt="">
                                <div class="overlay">
                                    <i class="fa fa-link"></i>
                                </div>
                            </div>
                            </a>
                        </li>

                        <li  class="portfolio filter spray">
                            <a href="{!! url('portfolio/details') !!}">
                            <div class="post_thumb">
                                <img src="{!! asset('/assets') !!}/img/blog/b1.jpg" alt="">
                                <div class="overlay">
                                    <i class="fa fa-link"></i>
                                </div>
                            </div>
                            </a>
                        </li>

                        <li class="portfolio filter spray">
                            <div class="post_thumb">
                                <img src="{!! asset('/assets') !!}/img/blog/b1.jpg" alt="">
                                <div class="overlay">
                                    <i class="fa fa-link"></i>
                                </div>
                            </div>
                        </li>

                        <li  class="portfolio filter irrigation">
                            <div class="post_thumb">
                                <img src="{!! asset('/assets') !!}/img/blog/b1.jpg" alt="">
                                <div class="overlay">
                                    <i class="fa fa-link"></i>
                                </div>
                            </div>
                        </li>

                        <li  class="portfolio filter irrigation">
                            <div class="post_thumb">
                                <img src="{!! asset('/assets') !!}/img/blog/b1.jpg" alt="">
                                <div class="overlay">
                                    <i class="fa fa-link"></i>
                                </div>
                            </div>
                        </li>

                        <li  class="portfolio filter irrigation">
                            <div class="post_thumb">
                                <img src="{!! asset('/assets') !!}/img/blog/b1.jpg" alt="">
                                <div class="overlay">
                                    <i class="fa fa-link"></i>
                                </div>
                            </div>
                        </li>

                    </ul><!--end portfolio_items-->

                </div>

                <div class="col-md-12 text-center">
                    <nav aria-label="Page navigation" class="mt-30">
                        <ul class="pagination flex justify-content-center" id="blog-post-sidebar">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span class="fa fa-long-arrow-alt-left"></span>
                                </a>
                            </li>
                            <li class="page-item active">
                                <a id="page-link1" class="page-link" href="#">01</a>
                            </li>
                            <li class="page-item">
                                <a id="page-link2" class="page-link" href="#">02</a>
                            </li>
                            <li class="page-item">
                                <a id="page-link3" class="page-link" href="#">03</a>
                            </li>
                            <li class="page-item">
                                <a id="page-link4" class="page-link" href="#">04</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span class="fa fa-long-arrow-alt-right"></span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
        </div>
    </section>



@endsection
@section('page_script')
    <script>
        $(document).ready(function(){

            $(".filter-button").click(function(){
                var value = $(this).attr('data-filter');

                if(value == "all")
                {
                    //$('.filter').removeClass('hidden');
                    $('.filter').show();
                }
                else
                {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                    $(".filter").not('.'+value).hide('500');
                    $('.filter').filter('.'+value).show('500');

                }
            });

            if ($(".filter-button").removeClass("active")) {
                $(this).removeClass("active");
            }
            $(this).addClass("active");

        });
    </script>
@endsection
