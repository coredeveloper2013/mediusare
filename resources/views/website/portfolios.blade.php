@extends('website.master')

@section('title')
    Mediusware | Portfolios
@endsection

@section('content')
    <!-- Start page-top section -->
    <section class="page-top-section">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6 col-md-6">
                    <h1 class="text-white">Portfolio</h1>
                </div>
                <div class="col-lg-6  col-md-6 page-top-nav">
                    <div>
                        <a href="{!! url('/home') !!}">Home</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="teams.html">Portfolio</a>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- End page-top section -->

    <section>
        <div class="container">
            <div class="grid-list-wrap">
                <article
                        class="grid server-based-mobile-apps utility-mobile-apps post-29216 portfolio type-portfolio status-publish has-post-thumbnail hentry portfolio-category-server-based-mobile-apps portfolio-category-utility-mobile-apps loaded"
                        itemscope="" itemtype="http://schema.org/Article"
                        style="transform: translate(0px, 0px); opacity: 1;">

                    <div class="grid-inner">

                        <figure class="grid-list-image">
                            <a href="https://portfolio.granjur.com/portfolio/performance360/"
                               class="grid-list-image-link grid-list-page">
									<span class="grid-list-image-placeholder" style="padding-top: 90.0188323917%;">
										<img width="531" height="478"
                                             src="https://portfolio.granjur.com/wp-content/uploads/2018/03/p360-531x478.jpg?x65408"
                                             class="attachment-helium_portfolio_thumb_4by3 size-helium_portfolio_thumb_4by3 wp-post-image"
                                             alt="p360" itemprop="image"
                                             srcset="https://portfolio.granjur.com/wp-content/uploads/2018/03/p360-531x478.jpg 531w, https://portfolio.granjur.com/wp-content/uploads/2018/03/p360-300x270.jpg 300w, https://portfolio.granjur.com/wp-content/uploads/2018/03/p360-768x691.jpg 768w, https://portfolio.granjur.com/wp-content/uploads/2018/03/p360-720x648.jpg 720w, https://portfolio.granjur.com/wp-content/uploads/2018/03/p360.jpg 1000w"
                                             sizes="(max-width: 531px) 100vw, 531px">									</span>
                                <span class="overlay"></span>
                            </a>
                        </figure>

                        <div class="portfolio-info">
                            <a class="portfolio-info-link"
                               href="https://portfolio.granjur.com/portfolio/performance360/" title="Performance360 PPT"
                               itemprop="url">
                                <h3 class="entry-title portfolio-title" itemprop="name">Performance360 PPT</h3>
                                <p class="portfolio-meta">
                                    Server Based Mobile Apps, Utility Mobile Apps </p>
                            </a>
                        </div>

                    </div>

                    <div class="grid-loader">
                        <div class="helium-loader"></div>
                    </div>

                </article>

            </div>
        </div>
    </section>



@endsection
@section('page_script')
    <script>
        $(document).ready(function(){

            $(".filter-button").click(function(){
                var value = $(this).attr('data-filter');

                if(value == "all")
                {
                    //$('.filter').removeClass('hidden');
                    $('.filter').show();
                }
                else
                {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                    $(".filter").not('.'+value).hide('500');
                    $('.filter').filter('.'+value).show('500');

                }
            });

            if ($(".filter-button").removeClass("active")) {
                $(this).removeClass("active");
            }
            $(this).addClass("active");

        });
    </script>
@endsection