@extends('website.master')

@section('title')
    Mediusware | Portfolios
@endsection

@section('content')
    <!-- Start page-top section -->
    <section class="page-top-section">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6 col-md-6">
                    <h1 class="text-white">Portfolio Details</h1>
                </div>
                <div class="col-lg-6  col-md-6 page-top-nav">
                    <div>
                        <a href="{!! url('/home') !!}">Home</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="teams.html">Portfolio Details</a>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- End page-top section -->



    <!-- Portfolio Details -->
    <section class="bl-panel-items" id="bl-panel-work-items">
        <div class="container">
            <div class="row justify-content-center section-title-wrap" style="padding: 50px 0">
                <div class="col-lg-12">
                    <div class="title-img">
                        <img src="{!! asset('/assets') !!}/img/title-icon.png" alt="">
                    </div>
                    <h1>Portfolio<span class="text-info"> Details</span> </h1><br>
                    <p>Portfolio Title</p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-7">
                    <div class="project-thumb">
                        <img src="{!! asset('/assets') !!}/img/blog/b1.jpg" alt="" />
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-5">
                    <div class="project-desc">
                        <h2>Project Description</h2>
                        <p>Lorem ipsum Non dolor fugiat Duis irure esse eiusmod aliquip laborum dolor ea cupidatat nostrud do elit esse occaecat Duis sit.</p>
                        <p>Lorem ipsum Non dolor fugiat Duis irure esse eiusmod aliquip laborum dolor ea cupidatat nostrud do elit esse occaecat Duis sit.
                        <h2>Project Details</h2>
                        <ul>
                            <li><b>Skills: </b>PHP, JS, MySQL</li>
                            <li><b>Client: </b>Google, Inc</li>
                            <li><b>Date: </b>April 20, 2016</li>
                            <li><b>Category: </b><a href="#">Web Design</a></li>
                            <li><b>Project Url: </b><a href="#">www.example.net</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

    </section>
    <!-- End Portfolio Details -->




@endsection