<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Aimstar</title>

    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.logo {
            font-size: 45px;
            line-height: 45px;
            color: #333;
            text-align: center;
        }

        .invoice-box table tr.top table td.title {
            color: #333;
            text-align: center;
        }

        .invoice-box .product-img td{
            color: #333;
            vertical-align: middle
        }

        .invoice-box table tr.top table td.title2 {
            color: #333;
            text-align: center;
            margin: 0;
        }

        .invoice-box table tr.top table td.title2 h2{
            margin: 0;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }


        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }



        .invoice-box table tr.total td:nth-child(2) {
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .product-img td img{
                width: 200px!important;
            }
        }

        @media only screen and (max-width: 360px) {
            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box {
                width: 364px!important;
                margin-left: -5px;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .product-img td img{
                width: 200px!important;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
    </style>
</head>

<body>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="logo" colspan="2">
                            <img src="{!! asset('logo.png') !!}" style="width:100%; max-width:300px;">
                        </td>

                    </tr>
                </table>
            </td>
        </tr>

        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title" colspan="2">
                            Hello Action2
                        </td>

                    </tr>
                </table>
            </td>
        </tr>

        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title2" colspan="2">
                            <h2>Your item has been sold on the marketplace!</h2>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title" colspan="2">
                            <b>€0.72</b> has been added to your balance.
                        </td>

                    </tr>
                </table>
            </td>
        </tr>

        <tr class="item">
            <td colspan="2" style="text-align: right">
                SALE FEE:<br>
                <b>€0.3</b>
            </td>
        </tr>

        <tr class="item">
            <td>
                2018-11-07 11:46:29
            </td>
            <td  style="text-align: right">
                YOU RECEIVE:<br>
                <b>€0.72</b>
            </td>
        </tr>

        <tr class="item">
            <td>
                MARKETSELL ID: <b>2</b><br>
                TYPE: <b>MARKETSELL TRANSACTION</b>
            </td>

            <td>
                TOTAL PRICE<br>
                ITEMS: <b>€0.75</b>
            </td>
        </tr>

        <tr class="product-img" >
            <td style="text-align: right">
                <img src="{!! asset('coin.png') !!}" width="300">
            </td>

            <td style="text-align: left">
                1x <b>€0.75</b>
            </td>
        </tr>

        <tr class="item">
            <td>
                <b>StatTrak<sup>TM</sup>P90 | Grim (Well-Worn)</b>
            </td>

        </tr>

    </table>
</div>
</body>
</html>
