<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
    <meta charset="utf-8">
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{!! asset('/assets/img/favicon.png') !!}">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Mediusware</title>
{{--    <meta charset="UTF-8">--}}
    <meta name="author" content="Mediusware">
    <meta name="description" content="Have a look, what we done for our clients!">
    <meta name="keywords" content="mediusware,portfolio,web development, laravel,html,css,vuejs">

    <meta property="og:title" content="Mediusware"/>
    <meta property="og:url" content="https://mediusware.com"/>
    <meta property="og:description" content="Have a look, what we done for our clients!"/>
    <meta property="og:image" content="{{ asset('assets/img/share-banner.png') }}"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="x-ua-compatible" content="IE=edge">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,500,600,700" rel="stylesheet">

    <link rel="preload" href="{{ mix('assets/css/all.css') }}" as="style">
    <link rel="preload" href="{{ mix('assets/css/custom.css') }}" as="style">

    <link rel="preload" href="{{ asset('js/app.js') }}" as="script">
    <link rel="preload" href="{{ mix('assets/js/all.js') }}" as="script">

    <link href="{{ mix('assets/css/all.css') }}" rel="stylesheet">
    <link href="{{ mix('assets/css/custom.css') }}" rel="stylesheet">


<style>
#header {
    padding: 10px 0 !important;
}
</style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-177278831-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-177278831-1');
</script>

</head>

<body class="version8">

<!-- Start main content -->
<div id="app">
    <my-header></my-header>
    <div class="body-content">
        <router-view></router-view>
    </div>
    <my-footer></my-footer>
</div>
<!-- End main content -->

<script ref="" src="{{ asset('js/app.js') }}" defer></script>
<script src="{{ mix('assets/js/all.js') }}" defer></script>
<script>
    $.fn.andSelf = function () {
        return this.addBack.apply(this, arguments);
    }
</script>
@yield('page_script')
</body>
</html>
