-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2020 at 05:43 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mediusware`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `slug`, `title`, `description`, `image`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(2, 'our-vision', 'Our Vision', 'To become a prime performer, in providing quality Web and Software solutions in the competitive global market place.', '1562991590.png', 'active', 2, '2019-07-12 22:19:50', '2019-07-12 22:19:50'),
(3, 'our-commitment', 'Our Commitment', 'We take pride in our on time delivery and ability to meet quick turn around requests while exceeding customer quality demands. Customer Satisfaction continues to be of utmost importance , as do Consistent quality, Constant innovation, Technology enhancement, Process improvement and Customer orientation', '1562991606.png', 'active', 3, '2019-07-12 22:20:06', '2019-07-12 22:20:06'),
(4, 'our-mission', 'Our Mission', 'To enhancing the business growth of our customers with creative Design and Development to deliver market-defining high-quality solutions that create value and consistent competitive advantage for our clients around the world.', '1563014257.png', 'active', 1, '2019-07-13 04:37:37', '2019-07-13 04:37:37');

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacancy` tinyint(4) DEFAULT NULL,
  `job_context` text COLLATE utf8mb4_unicode_ci,
  `job_responsibilities` longtext COLLATE utf8mb4_unicode_ci,
  `employment_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `experience_requirements` text COLLATE utf8mb4_unicode_ci,
  `additional_requirements` longtext COLLATE utf8mb4_unicode_ci,
  `salary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `apply_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id`, `slug`, `title`, `vacancy`, `job_context`, `job_responsibilities`, `employment_status`, `experience_requirements`, `additional_requirements`, `salary`, `deadline`, `apply_email`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'web-developer-laravel-vuejs', 'Web Developer (Laravel & VueJs)', 5, 'We are looking for PHP developers who are experienced with Laravel/ CodeIgniter HMVC/ MVC architecture and working with APIs.', '<p><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Develop, maintain, and implement a broad range of web and mobile based client tools including Content Management Systems, media &amp; entertainment custom applications</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Develop and modify databases, business logic, and front-end solutions.</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Ability to accurately specify timelines</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Attention to detail, We like to do it right the first time</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Standard coding practice</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Work with supporting team members to ensure quality execution</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Study and apply the latest technology and design trends; suggest approaches to improve project and team outcomes</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Full stack web application development</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Designing interactive web pages.</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Work in strong agile teams, using modern development methodologies.</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Work closely with the team to deliver impactful product initiatives.</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Ensuring cross-platform optimization for mobile phones.</span><br></p>', 'Full Time', '2 to 5 year(s)', '<p><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Age 22 to 35 years</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Both males and females are allowed to apply</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- 2-5 yrs experience with PHP and MySQL</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- In-depth knowledge of HTML and CSS. Ability to hand-code HTML and CSS</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Very experienced with MVC architecture / OOP PHP programming</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Basic understanding of Responsive CSS Layouts</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Experience with Javascript and JQuery</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Self-reliant; able to work independently and accept responsibility for projects</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Experience developing for a LAMP development (.htaccess)</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Strong problem solving and analytical skills</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Knowledge in CodeIgniter, Bootstrap 3,4 are plus</span><br style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\"><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">- Experience in API development for iOS or Android app</span><br></p>', '25k - 30k', '2020-02-29', 'rony@gmail.com', '1582716599.png', 'active', '2020-02-26 05:29:59', '2020-02-26 21:55:58'),
(3, 'software-developer', 'Software Developer', 3, 'We are looking for talented serious individuals with quick learning skills, dedication towards job and someone who works responsibly in a team.', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 40px; padding: 0px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif, solaimanlipi;\"><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Full life cycle application development</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Must have good analytic skill and developing web &amp; C#, ASP.NET MVC &amp; MS SQL Server</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Good knowledge in Microsoft reporting service, crystal report design &amp; development</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Good understanding of front-end technologies, including HTML5, CSS3, JavaScript, JQuery</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Writes code optimization</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Front end graphical user interface design</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Writes and runs unit tests</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Maintain and document software functionality</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Document the life cycle of projects by creating/writing documentation, flowcharts, layouts, diagrams, charts, code comments, etc.</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Integrate software with existing systems</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Evaluate and identify new technologies for implementation</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Analyzing user requirements</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Developing existing programs by analyzing and identifying areas for modification</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Maintaining systems by monitoring and correcting software defects</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Consulting clients/colleagues concerning the maintenance and performance of software systems and asking questions to obtain information, clarify details and implement information</li></ul>', 'Full Time', '3 to 5 year(s)', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 40px; padding: 0px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif, solaimanlipi;\"><li class=\"bn\" style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px; font-family: solaimanlipi, sans-serif !important; font-size: 15px !important;\">Age 25 to 40 years</li><li class=\"bn\" style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px; font-family: solaimanlipi, sans-serif !important; font-size: 15px !important;\">Both males and females are allowed to apply</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">MUST have clear understanding on Object Oriented Design and Analysis</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Excellent capability of writing complex SQL queries, Views and Stored Procedures.</li></ul><h5 style=\"font-family: Arial, Helvetica, sans-serif, solaimanlipi; font-weight: bold; line-height: 1.1; color: rgb(92, 92, 92); margin: 20px 0px 6px; font-size: 14px;\">Compensation &amp; Other Benefits</h5><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 40px; padding: 0px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif, solaimanlipi;\"><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Salary Review: Yearly</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Festival Bonus: 2</li></ul>', '25k - 30k', '2020-02-29', 'rony@gmail.com', NULL, 'active', '2020-02-26 21:46:25', '2020-02-26 21:46:25'),
(4, 'full-stack-developer-laravel', 'Full Stack Developer (Laravel)', 10, 'EATL is a leading ICT and software development company working with the government/autonomous bodies, private organizations, INGOs, donors and some world most renowned IT expertise organizations. We are urgently hiring for Full Stack (Laravel 5.6/+ and Angular 6/+) Developer. This is a technical position to provide technical solutions in delivering business outcomes with team.', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 40px; padding: 0px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif, solaimanlipi;\"><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Work on Web Application Development Projects with Laravel 5.6/+, Angular(6/+)</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">You will be responsible to work as active team member for requirement analysis, system design, database design and preparing software requirement specifications considering both high level to low level concept;</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Understanding the architecture and write scalable code for Web and REST API;</li></ul>', 'Full Time', '5 to 7 year(s)', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 40px; padding: 0px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif, solaimanlipi;\"><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">3 to 5 years of experience in software development using Laravel and Angular.</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Strong Knowledge of object oriented programming;</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Experienced in ReactJs, VueJS.</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Experiences in NodeJS programming will be added plus point;</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Familiarity with JavaScript build tools such as NPM/Yarn.</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Familiarity with Agile (Scrum) process and project management tools like JIRA;</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Familiarity with SQL/No-SQL data stores: Redis, MongoDB, ElasticSearch.</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Solid understanding of using versioning tools, e.g. Git, BitBucket, Mercurial or SVN;</li><li style=\"color: rgb(92, 92, 92); line-height: 24px; padding-bottom: 5px;\">Experience with Linux is required, and AWS experience is a plus.</li></ul>', '75k - 100k', '2020-03-10', 'rony@gmail.com', '1582775746.jpg', 'active', '2020-02-26 21:55:46', '2020-02-26 21:55:46');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `slug`, `title`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'laravel', 'Laravel', 'For All laravel Application', 'active', '2019-07-01 04:46:10', '2019-07-03 03:42:48'),
(2, 'wordpress', 'WordPress', 'For WordPress website', 'active', '2019-07-02 23:39:25', '2019-07-03 03:43:02'),
(3, 'psd-to-html-css', 'PSD to HTML/CSS', 'For PSD to HTML CSS design website', 'active', '2019-07-02 23:40:20', '2019-07-03 03:43:05'),
(4, 'javascript', 'JavaScript', 'For JavaScript website', 'active', '2019-07-02 23:41:03', '2019-07-03 03:43:08'),
(5, 'vue-js', 'Vue Js', 'For Vue Js website', 'active', '2019-07-02 23:49:47', '2019-07-03 03:43:12');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `title`, `image`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(2, 'client 1', '1563168597.png', 'active', 1, '2019-07-13 00:58:57', '2019-07-14 23:29:57'),
(3, 'client 2', '1563168665.png', 'active', 2, '2019-07-13 00:59:07', '2019-07-14 23:31:05'),
(4, 'client 3', '1563168820.png', 'active', 0, '2019-07-14 23:33:40', '2019-07-14 23:33:40'),
(5, 'client 4', '1563168838.png', 'active', 0, '2019-07-14 23:33:58', '2019-07-14 23:33:58'),
(6, 'client 5', '1563168851.png', 'active', 0, '2019-07-14 23:34:11', '2019-07-14 23:34:11'),
(7, 'client 6', '1563168863.png', 'active', 0, '2019-07-14 23:34:23', '2019-07-14 23:34:23'),
(8, 'client 7', '1563168881.png', 'active', 0, '2019-07-14 23:34:41', '2019-07-14 23:34:41'),
(9, 'client 8', '1563168900.png', 'active', 0, '2019-07-14 23:35:00', '2019-07-14 23:35:00'),
(10, 'client 9', '1563168921.png', 'active', 0, '2019-07-14 23:35:21', '2019-07-14 23:35:21'),
(11, 'client 10', '1563168934.png', 'active', 0, '2019-07-14 23:35:34', '2019-07-14 23:35:34');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `subject`, `message`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, 'Rony', 'rony@gmail.com', 'jsd hkjdsh fkjasdh fjkhasd jfha kdjfh d', 'kjadsh fkjadsh fkjhsdf kjasdh kfj hasdkfhjkasdhfashdfkhadsjhb   ksdahfk asdk hfads\nadslkjfa sdfkhad kfahsd', '127.0.0.1', '2019-07-04 21:53:26', '2019-07-04 21:53:26'),
(2, 'Rony', 'rony@gmail.com', 'asdf', 'kjadsh fkjadsh fkjhsdf kjasdh kfj hasdkfhjkasdhfashdfkhadsjhb   ksdahfk asdk hfads\nadslkjfa sdfkhad kfahsd', '127.0.0.1', '2019-07-04 21:53:43', '2019-07-04 21:53:43'),
(3, 'Rony', 'rony@gmail.com', 'ads', 'adsf ds', '127.0.0.1', '2019-07-04 21:55:24', '2019-07-04 21:55:24'),
(4, 'Rony', 'rony@gmail.com', 'asdf adsf', 'adsf adsf', '127.0.0.1', '2019-07-04 21:59:07', '2019-07-04 21:59:07'),
(5, 'Xuher Anzum', 'rony@gmail.com', 'asdf', 'asdf', '127.0.0.1', '2019-07-04 21:59:33', '2019-07-04 21:59:33'),
(6, 'Xuher Anzum', 'rony@gmail.com', 'asdf', 'asdf', '127.0.0.1', '2019-07-04 21:59:41', '2019-07-04 21:59:41'),
(8, 'Xuher Anzum', 'zulkawsar@gmail.com', 'afa dfadsf', 'adsf adsf adsf adsf', '127.0.0.1', '2019-07-04 22:06:14', '2019-07-04 22:06:14'),
(9, 'adsf', 'rony@gmail.com', 'adf', 'adsf', '127.0.0.1', '2019-07-04 22:07:47', '2019-07-04 22:07:47'),
(10, 'Rony', 'rony@gmail.com', 'asdf', 'asdf', '127.0.0.1', '2019-07-04 22:07:56', '2019-07-04 22:07:56'),
(11, 'Rony', 'rony@gmail.com', 'adf', 'adsfads', '127.0.0.1', '2019-07-04 22:08:33', '2019-07-04 22:08:33'),
(12, 'Rony', 'rony@gmail.com', 'asdf ad', 'adsf d', '127.0.0.1', '2019-07-04 22:09:00', '2019-07-04 22:09:00'),
(13, 'Rony', 'zulkawsar@gmail.com', 'adf', 'adsf', '127.0.0.1', '2019-07-04 22:12:55', '2019-07-04 22:12:55'),
(14, 'Xuher Anzum', 'zulkawsar@gmail.com', 'kajsdf hkjasdh fkjasdh fjdsf', 'asdfadsf dsaf', '127.0.0.1', '2019-07-04 22:29:39', '2019-07-04 22:29:39'),
(15, 'Xuher Anzum', 'zulkawsar@gmail.com', 'asdf adf', 'asdf adsf adsf', '127.0.0.1', '2019-07-04 22:34:01', '2019-07-04 22:34:01'),
(16, 'Xuher Anzum', 'rony@gmail.com', 'asdf ad', 'asdf asdf asdf', '127.0.0.1', '2019-07-04 22:34:47', '2019-07-04 22:34:47'),
(17, 'Xuher Anzum', 'zulkawsar@gmail.com', 'asdfsd', 'asdfadsfasdf', '127.0.0.1', '2019-07-05 02:38:12', '2019-07-05 02:38:12'),
(18, 'Rashiqul Rony', 'rony@gmail.com', 'If you are looking to get in touch with the medical team, please log in to the app', 'If you are looking to get in touch with the medical team, please log in to the appIf you are looking to get in touch with the medical team, please log in to the app If you are looking to get in touch with the medical team, please log in to the app If you are looking to get in touch with the medical team, please log in to the app', '127.0.0.1', '2019-07-05 21:29:23', '2019-07-05 21:29:23'),
(19, 'Rony', 'rony@gmail.com', 'asd asdf', 'asdf asdf', '127.0.0.1', '2019-07-29 23:41:26', '2019-07-29 23:41:26'),
(20, 'Rony', 'rony@gmail.com', 'ad asdf', 'adf df', '127.0.0.1', '2019-07-29 23:44:54', '2019-07-29 23:44:54'),
(21, 'Rony', 'rony@gmail.com', 'fgsd', 'sdfg', '127.0.0.1', '2019-07-31 05:39:18', '2019-07-31 05:39:18'),
(22, 'Xuher Anzum', 'rony@gmail.com', 'Aimstar Mail Test', 'alsdk jflaksdjf lakjd alksdjf asjdfaksdjf kkjasd', '127.0.0.1', '2019-08-02 23:50:01', '2019-08-02 23:50:01'),
(23, 'Rony', 'rony@gmail.com', 'Aimstar Mail', 'alsdk jflaksdjf lakjd alksdjf asjdfaksdjf kkjasd', '127.0.0.1', '2019-08-02 23:51:46', '2019-08-02 23:51:46'),
(24, 'Xuher Anzum', 'rony@gmail.com', 'adfga a', 'adsf asdf dsf', '127.0.0.1', '2019-08-03 00:13:02', '2019-08-03 00:13:02'),
(25, 'Rony', 'rony@gmail.com', 'adfga a', 'adsf asdf dsf', '127.0.0.1', '2019-08-03 00:15:00', '2019-08-03 00:15:00'),
(26, 'Xuher Anzum', 'faizanali2012a@gmail.com', 'NEw', 'Share us with your ideas or send us your query, and please be patient our support team is certainly going to reach out to you. We have dedicated support team at your service for 24/7. So don’t worry.', '127.0.0.1', '2019-08-03 00:25:41', '2019-08-03 00:25:41'),
(27, 'Rony', 'rony@gmail.com', 'asdf', 'asdf asdf', '127.0.0.1', '2019-08-03 03:37:17', '2019-08-03 03:37:17');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `slug`, `title`, `short_description`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 'clean-a-hacked-wordpress-site-using-wordfence', 'Clean a Hacked WordPress Site using Wordfence', 'If you are running WordPress and you have been hacked, you can use Wordfence to clean much of the malicious code from your site. Wordfence lets you compare your hacked files against the original WordPress core files, and the original copies of WordPress themes and plugins in the repository.', '<p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-size: 15px; color: rgb(96, 96, 96); line-height: 1.4; font-family: Roboto, Arial, sans-serif;\">If your site has been hacked, <span style=\"box-sizing: inherit; font-weight: 700;\">Don’t Panic.</span></p><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-size: 15px; color: rgb(96, 96, 96); line-height: 1.4; font-family: Roboto, Arial, sans-serif;\">This article will describe how to clean your site if it has been hacked and infected with malicious code, backdoors, spam, malware or other nastiness. <span style=\"box-sizing: inherit; font-weight: 700;\">This article was updated on Friday March 20th, 2015</span> with additional tools you can use to clean your site. This article is written by Mark Maunder, the founder of Wordfence. I’m also an accredited security researcher, WordPress developer and I own and operate many of my own WordPress powered websites including this one. Even if you aren’t running WordPress, this article includes several tools that you can use to help clean your site from an infection.</p><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-size: 15px; color: rgb(96, 96, 96); line-height: 1.4; font-family: Roboto, Arial, sans-serif;\">If you are running WordPress and you have been hacked, you can use Wordfence to clean much of the malicious code from your site. Wordfence lets you compare your hacked files against the original WordPress core files, and the original copies of WordPress themes and plugins in the repository. Wordfence lets you see what has changed (do a diff) and gives you the option to repair files with one click and take other actions.</p>', '1562242418.jpg', 'active', '2019-07-04 01:02:26', '2019-07-04 06:13:38'),
(3, 'a-complete-guide-to-grid', 'A Complete Guide to Grid', 'CSS Grid Layout is the most powerful layout system available in CSS. It is a 2-dimensional system, meaning it can handle both columns and rows, unlike flexbox which is largely a 1-dimensional system.', '<p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-size: 15px; color: rgb(96, 96, 96); line-height: 1.4; font-family: Roboto, Arial, sans-serif;\">CSS Grid Layout (aka “Grid”), is a two-dimensional grid-based layout system that aims to do nothing less than completely change the way we design grid-based user interfaces. CSS has always been used to lay out our web pages, but it’s never done a very good job of it. First we used tables, then floats, positioning and inline-block, but all of these methods were essentially hacks and left out a lot of important functionality (vertical centering, for instance). Flexbox helped out, but it’s intended for simpler one-dimensional layouts, not complex two-dimensional ones (Flexbox and Grid actually work very well together). Grid is the very first CSS module created specifically to solve the layout problems we’ve all been hacking our way around for as long as we’ve been making websites.</p><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-size: 15px; color: rgb(96, 96, 96); line-height: 1.4; font-family: Roboto, Arial, sans-serif;\">There are two primary things that inspired me to create this guide. The first is Rachel Andrew’s awesome book <a href=\"http://abookapart.com/products/get-ready-for-css-grid-layout\" style=\"box-sizing: inherit; color: rgb(48, 139, 195);\">Get Ready for CSS Grid Layout.</a> It’s a thorough, clear introduction to Grid and is the basis of this entire article. I <em style=\"box-sizing: inherit;\">highly</em> encourage you to buy it and read it. My other big inspiration is Chris Coyier’s <a href=\"https://css-tricks.com/snippets/css/a-guide-to-flexbox/\" style=\"box-sizing: inherit; color: rgb(48, 139, 195);\">A Complete Guide to Flexbox</a>, which has been my go-to resource for everything flexbox. It’s helped a ton of people, evident by the fact that it’s the top result when you Google “flexbox.” You’ll notice many similarities between his post and mine, because why not steal from the best?</p><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-size: 15px; color: rgb(96, 96, 96); line-height: 1.4; font-family: Roboto, Arial, sans-serif;\">My intention with this guide is to present the Grid concepts as they exist in the very latest version of the specification. So I won’t be covering the out of date IE syntax, and I’ll do my best to update this guide regularly as the spec matures.</p>', '1562242741.jpg', 'active', '2019-07-04 01:03:35', '2019-07-04 06:19:01'),
(4, 'google-font-api', 'Google Font API', 'You essentially hotlink directly to CSS files on Google.com. Through URL parameters, you specificity which fonts you want, and what variations of those fonts.', '<div class=\"ffb-id-p76bbao post-content ff-post-content-element fg-text-dark\" style=\"box-sizing: inherit; color: rgb(51, 51, 51); font-family: Roboto, Arial, sans-serif;\"><div class=\"post-content ff-richtext\" style=\"box-sizing: inherit; font-size: 15px; color: rgb(96, 96, 96); line-height: 1.4;\"><article id=\"post-6602\" style=\"box-sizing: inherit;\"><div class=\"article-content\" style=\"box-sizing: inherit;\"><h4 style=\"box-sizing: inherit; line-height: 1.4; color: rgb(52, 52, 60); margin-right: 0px; margin-bottom: 15px; margin-left: 0px; font-size: 18px;\">Link to CSS files</h4><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; line-height: 1.4;\">You essentially hotlink directly to CSS files on Google.com. Through URL parameters, you specificity which fonts you want, and what variations of those fonts.</p><pre style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; font-size: 13px; padding: 9.5px; margin-bottom: 10px; line-height: 1.42857; color: rgb(51, 51, 51); word-break: break-all; overflow-wrap: break-word; background-color: rgb(245, 245, 245); border: 1px solid rgb(204, 204, 204); border-radius: 4px;\"><code class=\"language-markup\" style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; padding: 0px; background-color: transparent; border-radius: 0px; white-space: pre-wrap;\"><head>\r\n  \r\n   ...\r\n\r\n   <link rel=\"stylesheet\" type=\"text/css\" href=\"http://fonts.googleapis.com/css?family=Tangerine:bold,bolditalic|Inconsolata:italic|Droid+Sans\">\r\n</head></code></pre><div class=\"explanation\" style=\"box-sizing: inherit;\"><span style=\"box-sizing: inherit; font-weight: 700;\">Idea:</span> You can avoid an extra network request by opening that stylesheet and copy-and-pasting the @font-face stuff into your main stylesheet. But beware: Google does some User Agent sniffing stuff to sometimes serve different things to different devices as needed. You won’t benefit from that if done this way.</div><h4 style=\"box-sizing: inherit; line-height: 1.4; color: rgb(52, 52, 60); margin-right: 0px; margin-bottom: 15px; margin-left: 0px; font-size: 18px;\">CSS</h4><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; line-height: 1.4;\">In your CSS you can then specify these new fonts by name on any element you wish to use them.</p><pre style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; font-size: 13px; padding: 9.5px; margin-bottom: 10px; line-height: 1.42857; color: rgb(51, 51, 51); word-break: break-all; overflow-wrap: break-word; background-color: rgb(245, 245, 245); border: 1px solid rgb(204, 204, 204); border-radius: 4px;\"><code class=\"language-css\" style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; padding: 0px; background-color: transparent; border-radius: 0px; white-space: pre-wrap;\">body { \r\n  font-family: \'Tangerine\', \'Inconsolata\', \'Droid Sans\', serif; \r\n  font-size: 48px; \r\n}</code></pre><h4 style=\"box-sizing: inherit; line-height: 1.4; color: rgb(52, 52, 60); margin-right: 0px; margin-bottom: 15px; margin-left: 0px; font-size: 18px;\">FontLoader</h4><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; line-height: 1.4;\">You can use the FontLoader JavaScript instead of linking to the CSS. It has advantages, like controlling the Flash of Unstyled Text (FOUT), and also disadvantages, like the fact that the fonts won’t load for users with JavaScript off.</p><pre style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; font-size: 13px; padding: 9.5px; margin-bottom: 10px; line-height: 1.42857; color: rgb(51, 51, 51); word-break: break-all; overflow-wrap: break-word; background-color: rgb(245, 245, 245); border: 1px solid rgb(204, 204, 204); border-radius: 4px;\"><code class=\"language-markup\" style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; padding: 0px; background-color: transparent; border-radius: 0px; white-space: pre-wrap;\"><script src=\"http://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js\"></script>\r\n<script type=\"text/javascript\">\r\n  WebFont.load({\r\n    google: {\r\n      families: [\'Cantarell\']\r\n    }\r\n  });\r\n</script>\r\n<style type=\"text/css\">\r\n  .wf-loading h1 { visibility: hidden; }\r\n  .wf-active h1, .wf-inactive h1 { \r\n    visibility: visible; font-family: \'Cantarell\'; \r\n  }\r\n</style></code></pre><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; line-height: 1.4;\">Those class names, e.g <code style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; font-size: 13.5px; padding: 2px 4px; color: rgb(199, 37, 78); background-color: rgb(249, 242, 244); border-radius: 4px;\">.wf-loading</code> get applied to the <code style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; font-size: 13.5px; padding: 2px 4px; color: rgb(199, 37, 78); background-color: rgb(249, 242, 244); border-radius: 4px;\"><html></code> element. So notice when the fonts are “loading”, you can use that class name to hide the text. Then when they are shown, make them visible again. That is how FOUT is controlled.</p></div></article></div></div><section class=\"ffb-id-p76d66v comments fg-text-dark\" style=\"box-sizing: inherit; padding: 60px 0px 5px; margin: 0px -15px; color: rgb(51, 51, 51); font-family: Roboto, Arial, sans-serif;\"><div id=\"comments\" class=\"comments-area\" style=\"box-sizing: inherit;\"><div class=\"ark-comment-form fg-text-dark ffb-comment-form-1\" id=\"respond\" style=\"box-sizing: inherit;\"><div class=\"comment-respond\" style=\"box-sizing: inherit;\"></div></div></div></section>', '1562242285.png', 'active', '2019-07-04 01:05:11', '2019-07-04 06:11:25'),
(5, 'fluid-width-video', 'Fluid Width Video', 'IN A WORLD of responsive and fluid layouts on the web ONE MEDIA TYPE stands in the way of perfect harmony: video.', '<p><span style=\"color: rgb(96, 96, 96); font-family: Roboto, Arial, sans-serif; font-size: 15px;\">IN A WORLD of responsive and fluid layouts on the web ONE MEDIA TYPE stands in the way of perfect harmony: video. There are lots of ways in which video can be displayed on your site. You might be self hosting the video and presenting it via the HTML5 <video> tag. You might be using </span><a href=\"https://youtube.com/\" style=\"box-sizing: inherit; background-color: rgb(255, 255, 255); color: rgb(48, 139, 195); font-family: Roboto, Arial, sans-serif; font-size: 15px;\">YouTube</a><span style=\"color: rgb(96, 96, 96); font-family: Roboto, Arial, sans-serif; font-size: 15px;\"> or </span><a href=\"https://vimeo.com/\" style=\"box-sizing: inherit; background-color: rgb(255, 255, 255); color: rgb(48, 139, 195); font-family: Roboto, Arial, sans-serif; font-size: 15px;\">Vimeo</a><span style=\"color: rgb(96, 96, 96); font-family: Roboto, Arial, sans-serif; font-size: 15px;\"> which provides <iframe> code to display videos. You might be using </span><a href=\"https://viddler.com/\" style=\"box-sizing: inherit; background-color: rgb(255, 255, 255); color: rgb(48, 139, 195); font-family: Roboto, Arial, sans-serif; font-size: 15px;\">Viddler</a><span style=\"color: rgb(96, 96, 96); font-family: Roboto, Arial, sans-serif; font-size: 15px;\"> or </span><a href=\"https://blip.tv/\" style=\"box-sizing: inherit; background-color: rgb(255, 255, 255); color: rgb(48, 139, 195); font-family: Roboto, Arial, sans-serif; font-size: 15px;\">Blip.tv</a><span style=\"color: rgb(96, 96, 96); font-family: Roboto, Arial, sans-serif; font-size: 15px;\"> which provide nested object/embed tags to display a Flash player. In each of these scenarios, it is very common for a static width and height to be declared.</span></p><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-size: 15px; color: rgb(96, 96, 96); line-height: 1.4; font-family: Roboto, Arial, sans-serif;\"></p><pre style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; font-size: 13px; padding: 9.5px; margin-bottom: 10px; line-height: 1.42857; color: rgb(51, 51, 51); word-break: break-all; overflow-wrap: break-word; background-color: rgb(245, 245, 245); border: 1px solid rgb(204, 204, 204); border-radius: 4px;\"><code style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; padding: 0px; background-color: transparent; border-radius: 0px; white-space: pre-wrap;\"><video <span style=\"box-sizing: inherit; font-weight: 700;\">width=\"400\" height=\"300\"</span> ... </code></pre><pre style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; font-size: 13px; padding: 9.5px; margin-bottom: 10px; line-height: 1.42857; color: rgb(51, 51, 51); word-break: break-all; overflow-wrap: break-word; background-color: rgb(245, 245, 245); border: 1px solid rgb(204, 204, 204); border-radius: 4px;\"><code style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; padding: 0px; background-color: transparent; border-radius: 0px; white-space: pre-wrap;\"><iframe <span style=\"box-sizing: inherit; font-weight: 700;\">width=\"400\" height=\"300\"</span> ... </code></pre><pre style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; font-size: 13px; padding: 9.5px; margin-bottom: 10px; line-height: 1.42857; color: rgb(51, 51, 51); word-break: break-all; overflow-wrap: break-word; background-color: rgb(245, 245, 245); border: 1px solid rgb(204, 204, 204); border-radius: 4px;\"><code style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; padding: 0px; background-color: transparent; border-radius: 0px; white-space: pre-wrap;\"><object <span style=\"box-sizing: inherit; font-weight: 700;\">width=\"400\" height=\"300\" ...</span>\r\n      <embed <span style=\"box-sizing: inherit; font-weight: 700;\">width=\"400\" height=\"300\"</span> ... </code></pre><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-size: 15px; color: rgb(96, 96, 96); line-height: 1.4; font-family: Roboto, Arial, sans-serif;\">Guess what. Declaring static widths isn’t a good idea in fluid width environments. What if the parent container for that video shrinks narrower than the declared 400px? It will bust out and probably look ridiculous and embarrassing.</p><figure style=\"box-sizing: inherit; margin-bottom: 0px; color: rgb(96, 96, 96); font-family: Roboto, Arial, sans-serif; font-size: 15px;\"><img src=\"https://mediusware.com/wp-content/uploads/2017/08/breakout.png\" alt=\"breakout\" style=\"box-sizing: inherit; border: 0px; max-width: 100%; height: auto;\"><figcaption style=\"box-sizing: inherit;\">Simple and contrived, but still ridiculous and embrassing.</figcaption></figure><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-size: 15px; color: rgb(96, 96, 96); line-height: 1.4; font-family: Roboto, Arial, sans-serif;\">So can’t we just do this?</p><pre style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; font-size: 13px; padding: 9.5px; margin-bottom: 10px; line-height: 1.42857; color: rgb(51, 51, 51); word-break: break-all; overflow-wrap: break-word; background-color: rgb(245, 245, 245); border: 1px solid rgb(204, 204, 204); border-radius: 4px;\"><code style=\"box-sizing: inherit; font-family: \"Courier New\", Courier, monospace, Arial, sans-serif; padding: 0px; background-color: transparent; border-radius: 0px; white-space: pre-wrap;\"><video <span style=\"box-sizing: inherit; font-weight: 700;\">width=\"100%\"</span> ... </code></pre><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-size: 15px; color: rgb(96, 96, 96); line-height: 1.4; font-family: Roboto, Arial, sans-serif;\">Well, yep, you can. If you are using standard HTML5 video, That will make the video fit the width of the container. It’s important that you remove the height declaration when you do this, so that the aspect ratio of the video is maintained as it grows and shrinks, lest you get awkward “bars” to fill the empty space (unlike images, the actual video maintains it’s aspect ratio regardless of the size of the element). You can get there via CSS (and not worry about what’s declared in the HTML) like this:</p>', '1563171965.jpg', 'active', '2019-07-04 06:25:52', '2019-07-15 00:26:06');

-- --------------------------------------------------------

--
-- Table structure for table `helps`
--

CREATE TABLE `helps` (
  `id` int(11) NOT NULL,
  `content` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `helps`
--

INSERT INTO `helps` (`id`, `content`) VALUES
(1, '<h4><em>Vacancy:</em></h4>\r\n\r\n<h4>&nbsp;</h4>\r\n\r\n<h4><em>Job Responsibilities:</em></h4>\r\n\r\n<h4>&nbsp;</h4>\r\n\r\n<h4><em>Employment Status:</em></h4>\r\n\r\n<ul>\r\n	<li>\r\n	<h4>Full-time</h4>\r\n	</li>\r\n</ul>\r\n\r\n<h4><em>Educational Requirements:</em></h4>\r\n\r\n<h4>&nbsp;</h4>\r\n\r\n<h4><em>Experience Requirements:</em></h4>\r\n\r\n<h4>&nbsp;</h4>\r\n\r\n<h4><em>Additional Requirements:</em></h4>\r\n\r\n<h4>&nbsp;</h4>\r\n\r\n<h4><em>Job Location:</em></h4>\r\n\r\n<h4>&nbsp;</h4>\r\n\r\n<h4><em>Salary</em></h4>\r\n\r\n<h4>&nbsp;</h4>\r\n\r\n<h4><em>Compensation &amp; Other Benefits:</em></h4>\r\n\r\n<h4>&nbsp;</h4>');

-- --------------------------------------------------------

--
-- Table structure for table `informative_texts`
--

CREATE TABLE `informative_texts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `informative_texts`
--

INSERT INTO `informative_texts` (`id`, `title`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Never fear, that\'s why we are here. You can learn about our time proven process in our process section.', 'inactive', 0, '2019-07-16 03:56:27', '2019-07-18 01:38:35'),
(2, 'You can learn about our time proven process in our process section.', 'inactive', 0, '2019-07-16 04:04:53', '2019-07-18 01:38:39'),
(3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, possimus quaerat. Animi deleniti dolorem in labore modi quis quo voluptas! Adipisci aperiam ea eveniet fuga, molestias', 'active', 0, '2019-07-18 01:38:48', '2019-07-18 01:38:48');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_07_01_070014_create_services_table', 2),
(4, '2019_07_01_091054_create_portfolios_table', 3),
(5, '2019_07_02_060243_create_teams_table', 4),
(6, '2019_07_02_083631_create_sliders_table', 5),
(7, '2019_07_02_114637_create_features_table', 6),
(8, '2019_07_03_040929_create_settings_table', 7),
(9, '2019_07_05_034129_create_contacts_table', 8),
(10, '2019_07_11_091539_create_skills_table', 9),
(11, '2019_07_12_042555_create_newsletter_subscriptions_table', 10),
(13, '2019_07_12_105724_create_abouts_table', 11),
(14, '2019_07_13_035959_create_technologies_table', 12),
(16, '2019_07_16_093352_create_informative_texts_table', 13),
(17, '2019_07_26_055718_create_portfolio_category_relations_table', 14),
(18, '2019_07_27_103439_create_testimonials_table', 15),
(24, '2019_08_23_105224_create_partners_table', 16),
(25, '2019_12_21_061058_create_careers_table', 16);

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_subscriptions`
--

CREATE TABLE `newsletter_subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `newsletter_subscriptions`
--

INSERT INTO `newsletter_subscriptions` (`id`, `email`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, 'rony@gmail.com', '127.0.0.1', '2019-07-11 22:41:56', '2019-07-11 22:41:56'),
(2, 'roddny@gmail.com', '127.0.0.1', '2019-07-11 23:17:01', '2019-07-11 23:17:01'),
(3, 'faizanali2012a@gmail.com', '127.0.0.1', '2019-07-15 23:36:28', '2019-07-15 23:36:28'),
(4, 'sudip.mediusware@gmail.com', '127.0.0.1', '2019-07-16 05:15:08', '2019-07-16 05:15:08'),
(5, '99darkgreengt@gmail.com', '127.0.0.1', '2019-07-24 06:51:03', '2019-07-24 06:51:03'),
(6, 'rony1@gmail.com', '127.0.0.1', '2019-07-24 06:51:35', '2019-07-24 06:51:35');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `name`, `country`, `about`, `image`, `sort`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Md. Shahinur Rahman', 'Uganda', '<p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px; text-align: justify;\">Md. Shahinur Rahman is our Uganda partner since 2016. He provides our company with a comprehensive sourcing, planning and management methodology for website outsourcing.</p><p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px; text-align: justify;\">Some Key Projects:</p><p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px; text-align: justify;\">Laravel -<a href=\"https://app.bulk.ly/\" target=\"_blank\" style=\"color: rgb(0, 123, 255); transition: all 0.3s ease 0s;\">&nbsp;https://app.bulk.ly</a></p><p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px; text-align: justify;\">WordPress -&nbsp;<a href=\"http://aneco.io/\" target=\"_blank\" style=\"color: rgb(0, 123, 255); transition: all 0.3s ease 0s;\">http://aneco.io/</a></p>', '1582787195.jpg', 0, 'active', '2020-02-27 01:06:35', '2020-02-27 01:06:35');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `portfolios`
--

CREATE TABLE `portfolios` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_provided` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `industry` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_about` longtext COLLATE utf8mb4_unicode_ci,
  `solution` text COLLATE utf8mb4_unicode_ci,
  `impact` text COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `url` text COLLATE utf8mb4_unicode_ci,
  `youtube_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `showcase` tinyint(4) NOT NULL DEFAULT '0',
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolios`
--

INSERT INTO `portfolios` (`id`, `slug`, `title`, `client_name`, `service_provided`, `industry`, `company_about`, `solution`, `impact`, `date`, `url`, `youtube_id`, `image`, `showcase`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'virtual-affairs-a', 'Virtual Affairs a', 'William C.', 'Insurance Platform', 'Virtual Affairs', '<p>Virtual Affiars ia an international software and service company of more than 20 years of experience who are specialized in transforming digital channels for banks and insurance providers. They have flagship products like Insurance right, Banking Right and Investment Right.<br></p>', NULL, NULL, '2019-07-30', 'https://mediusware.com/', 'upRaaBPbuKs', '1564133712.jpg', 0, 'inactive', 0, '2019-07-26 03:35:12', '2019-07-30 05:33:50'),
(2, 'webcruiter', 'WebCruiter', 'Adelya Nh.', 'Recruitment Solution', 'Recruitment Agency', '<p>WebCruiter is a language-independent web-based recruitment solution provider in Norway. The solutions they provide is used by over 1,400 organizations with more than 65, 000 users in 80 countries.<br></p>', 'WebCruiter has no customer-facing recruitment management web application before. We developed a customer facing website and portal to manage their online order management journey.', 'We developed such a web application through which over 65,000 users from 50+ countries connected to WebCruiter. Our language-independent online recruitment application meet the needs for process support, provides quality assurance and makes the customers working day simpler and more efficient.', '2019-07-30', 'https://mediusware.com/', NULL, '1564134006.jpg', 1, 'active', 0, '2019-07-26 03:40:06', '2019-07-29 23:00:50');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_category_relations`
--

CREATE TABLE `portfolio_category_relations` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolio_category_relations`
--

INSERT INTO `portfolio_category_relations` (`id`, `portfolio_id`, `category_id`, `created_at`, `updated_at`) VALUES
(25, 3, 2, NULL, NULL),
(26, 3, 4, NULL, NULL),
(27, 4, 2, NULL, NULL),
(28, 4, 4, NULL, NULL),
(29, 5, 2, NULL, NULL),
(30, 5, 4, NULL, NULL),
(76, 6, 3, NULL, NULL),
(140, 2, 1, NULL, NULL),
(141, 2, 5, NULL, NULL),
(151, 1, 1, NULL, NULL),
(152, 1, 2, NULL, NULL),
(153, 1, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_features`
--

CREATE TABLE `portfolio_features` (
  `id` int(11) NOT NULL,
  `portfolio_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `portfolio_features`
--

INSERT INTO `portfolio_features` (`id`, `portfolio_id`, `title`, `created_at`, `updated_at`) VALUES
(117, 3, 'asdf asdf asdf asdf asdf asdf asdf', NULL, NULL),
(118, 3, 'Payroll Management', NULL, NULL),
(119, 3, 'System administration', NULL, NULL),
(120, 4, 'asdf asdf asdf asdf asdf asdf asdf', NULL, NULL),
(121, 4, 'Payroll Management', NULL, NULL),
(122, 4, 'System administration', NULL, NULL),
(123, 5, 'asdf asdf asdf asdf asdf asdf asdf', NULL, NULL),
(124, 5, 'Payroll Management', NULL, NULL),
(125, 5, 'System administration', NULL, NULL),
(248, 6, 'asdf asdf asdf asdf asdf asdf asdf', NULL, NULL),
(301, 2, 'CVs in multiple languages', NULL, NULL),
(302, 2, 'Create, resend, advert CV', NULL, NULL),
(303, 2, 'Creating and publishing job adverts', NULL, NULL),
(304, 2, 'Create and delete profiles', NULL, NULL),
(305, 2, 'Processing candidates and applications', NULL, NULL),
(306, 2, 'Adding attachments and references', NULL, NULL),
(307, 2, 'Administration of candidates', NULL, NULL),
(308, 2, 'Being searchable in the CV database (Open CV)', NULL, NULL),
(309, 2, 'Human Resource Management', NULL, NULL),
(310, 2, 'Communications center', NULL, NULL),
(311, 2, 'Report center', NULL, NULL),
(312, 2, 'System administration', NULL, NULL),
(343, 1, 'Sales Management', NULL, NULL),
(344, 1, 'Purchase Management', NULL, NULL),
(345, 1, 'Inventory Management', NULL, NULL),
(346, 1, 'Accounting ( advanced reporting system)', NULL, NULL),
(347, 1, 'Employee Directory', NULL, NULL),
(348, 1, 'Leave Management', NULL, NULL),
(349, 1, 'Employee Directory', NULL, NULL),
(350, 1, 'Human Resource Management', NULL, NULL),
(351, 1, 'Manufacturing & Production', NULL, NULL),
(352, 1, 'Payroll Management', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_images`
--

CREATE TABLE `portfolio_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_id` int(11) NOT NULL,
  `image` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolio_images`
--

INSERT INTO `portfolio_images` (`id`, `portfolio_id`, `image`, `created_at`, `updated_at`) VALUES
(4, 1, '1564464194_p1.png', NULL, NULL),
(5, 1, '1564464194_p2.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_technology_relations`
--

CREATE TABLE `portfolio_technology_relations` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_id` int(11) NOT NULL,
  `technology_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolio_technology_relations`
--

INSERT INTO `portfolio_technology_relations` (`id`, `portfolio_id`, `technology_id`, `created_at`, `updated_at`) VALUES
(39, 3, 6, NULL, NULL),
(40, 3, 8, NULL, NULL),
(41, 4, 6, NULL, NULL),
(42, 4, 8, NULL, NULL),
(43, 5, 6, NULL, NULL),
(44, 5, 8, NULL, NULL),
(134, 6, 6, NULL, NULL),
(174, 2, 5, NULL, NULL),
(175, 2, 7, NULL, NULL),
(176, 2, 14, NULL, NULL),
(177, 2, 15, NULL, NULL),
(199, 1, 4, NULL, NULL),
(200, 1, 5, NULL, NULL),
(201, 1, 6, NULL, NULL),
(202, 1, 7, NULL, NULL),
(203, 1, 8, NULL, NULL),
(204, 1, 9, NULL, NULL),
(205, 1, 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_left` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_right` longtext COLLATE utf8mb4_unicode_ci,
  `industries_image` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `image` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `slug`, `title`, `short_description`, `description_left`, `description_right`, `industries_image`, `date`, `image`, `icon`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'mobile-app-development', 'Mobile App Development', 'Developing innovative and native mobile Apps for Android, iOS, BlackBerry and Windows platforms.', '<p>With a large portfolio of industry leading mobile application development services, we know how to translate our solutions into the result of your business growth. We work on cross-platform, responsive mobile apps. Where others struggle, we thrive for the most attractive user interface and secure backend. Team Brain Station 23 provides the cutting-edge mobile app development service to diverse industries and clients including large scale organizations and startups similar to Uber.<br></p>', '<p>Our dedicated developers of different platforms build enterprise oriented mobile apps to best suit your business. Integrating trendy and smart notification features and secure payment gateways, we create a constant digital presence and enhance customer engagement for you. Our team has already developed customized mobile apps for the Banking, E-commerce industries and Augmented Reality or Virtual Reality platforms. We also have an amazingly dedicated team to develop mobile games.<br></p>', '1564209168.jpg', '2018-01-03', '1564209168.jpg', 'fab fa-android', 'active', 0, '2019-07-27 00:32:48', '2019-07-27 00:32:48'),
(2, 'banking-financial-solution', 'Banking & Financial Solution', 'Customized software solutions tailored for your unique banking & financial business needs.', '<p>Give your customers the freedom of banking from anywhere and anytime. We provide a complete range of the most secure software solutions developed for your banking business and financial institutions. Our offered solutions support mobile banking, online bill payments, remote access to accounts, online transactions and many more on-demand banking services you can ask for.<br></p>', '<p>Our futuristic solutions help empowering the financial institutions to be more competitive and develop lasting customer experience. By embracing the latest technologies, we build customized banking solutions and dedicated mobile apps to leverage your customer reach-ability and enhance flexibility.<br></p>', '1564211587.jpg', '2019-04-20', '1564211587.jpg', 'fas fa-money-bill-alt', 'active', 0, '2019-07-27 01:13:07', '2019-07-27 01:13:07');

-- --------------------------------------------------------

--
-- Table structure for table `service_industries`
--

CREATE TABLE `service_industries` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `services_industries_title` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_industries`
--

INSERT INTO `service_industries` (`id`, `service_id`, `services_industries_title`, `created_at`, `updated_at`) VALUES
(1, 1, 'E COMMERCE', NULL, NULL),
(2, 1, 'FIN-TECH', NULL, NULL),
(3, 1, 'TELECOMMUNICATION', NULL, NULL),
(4, 1, 'PHARMACEUTICALS', NULL, NULL),
(5, 1, 'HEALTH SECTOR', NULL, NULL),
(6, 1, 'LOCAL ENTERPRISES', NULL, NULL),
(7, 1, 'AGRICULTURAL', NULL, NULL),
(8, 1, 'MNC\'S', NULL, NULL),
(9, 1, 'START UPS', NULL, NULL),
(10, 2, 'BANKS', NULL, NULL),
(11, 2, 'FINANCIAL ORGANIZATION', NULL, NULL),
(12, 2, 'BANKING & FIN-TECH SOLUTION PROVIDERS', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_offered`
--

CREATE TABLE `service_offered` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `offered_services_title` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_offered`
--

INSERT INTO `service_offered` (`id`, `service_id`, `offered_services_title`, `created_at`, `updated_at`) VALUES
(1, 1, 'High-quality applications for a wide array of niche', NULL, NULL),
(2, 1, 'Attracting a larger user base & increasing business impact', NULL, NULL),
(3, 1, 'High standard functionality & features', NULL, NULL),
(4, 1, 'Enhancing visual appeal & rich user interface', NULL, NULL),
(5, 1, 'Cross-platform development for mobile apps', NULL, NULL),
(6, 1, 'Variety of app choices from native, web & hybrid', NULL, NULL),
(7, 1, 'Complete operating system support', NULL, NULL),
(8, 2, 'Retail Internet Banking', NULL, NULL),
(9, 2, 'Corporate Internet Banking', NULL, NULL),
(10, 2, 'Mobile Internet Banking', NULL, NULL),
(11, 2, 'Card Services', NULL, NULL),
(12, 2, 'Extensive Customization', NULL, NULL),
(13, 2, 'FDI Automation', NULL, NULL),
(14, 2, 'Internet Banking System', NULL, NULL),
(15, 2, 'Cyber Security and Audit', NULL, NULL),
(16, 2, 'Banking Process Automation', NULL, NULL),
(17, 2, 'Banking CRM', NULL, NULL),
(18, 2, 'Cloud Services for Banking', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_technology_relations`
--

CREATE TABLE `service_technology_relations` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `technology_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_technology_relations`
--

INSERT INTO `service_technology_relations` (`id`, `service_id`, `technology_id`, `created_at`, `updated_at`) VALUES
(1, 1, 9, NULL, NULL),
(2, 1, 11, NULL, NULL),
(3, 1, 12, NULL, NULL),
(4, 2, 4, NULL, NULL),
(5, 2, 5, NULL, NULL),
(6, 2, 6, NULL, NULL),
(7, 2, 7, NULL, NULL),
(8, 2, 8, NULL, NULL),
(9, 2, 10, NULL, NULL),
(10, 2, 12, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_2` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_3` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `map_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_details` text COLLATE utf8mb4_unicode_ci,
  `facebook_link` text COLLATE utf8mb4_unicode_ci,
  `twitter_link` text COLLATE utf8mb4_unicode_ci,
  `githtb_link` text COLLATE utf8mb4_unicode_ci,
  `stackoverflow_link` text COLLATE utf8mb4_unicode_ci,
  `linkedin_link` text COLLATE utf8mb4_unicode_ci,
  `pinterest_link` text COLLATE utf8mb4_unicode_ci,
  `youtube_link` text COLLATE utf8mb4_unicode_ci,
  `instagram_link` text COLLATE utf8mb4_unicode_ci,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_profile_pdf` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `short_name`, `address`, `email`, `phone`, `phone_2`, `phone_3`, `skype`, `map_url`, `home_title`, `home_details`, `facebook_link`, `twitter_link`, `githtb_link`, `stackoverflow_link`, `linkedin_link`, `pinterest_link`, `youtube_link`, `instagram_link`, `logo`, `cover_image`, `company_profile_pdf`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Mediusware', 'MW', 'Ring Road, Mohammadpur\r\nHouse- 18/5, Floor - 5th\r\nDhaka, Bangladesh', 'coredeveloper.2013@gmail.com', '+8801715712931', '+8801715712931', '+8801715712931', 'mediusware', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14605.680045424364!2d90.35596398728026!3d23.768053392393437!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa64a90f7a9e6475b!2sMediusware!5e0!3m2!1sbn!2sbd!4v1562238655793!5m2!1sbn!2sbd', 'We Made our  Software 100% Errorless', '<p><span style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">The first is a non technical method which requires the use of adware removal software. Download free adware and spyware removal software and use advanced tools to help prevent getting infected.</span><br></p>', 'https://www.facebook.com/', 'https://twitter.com/mediusware', 'https://github.com/', 'https://stackoverflow.com/', 'https://www.linkedin.com/', 'https://www.pinterest.com/', 'https://www.youtube.com/', NULL, NULL, NULL, NULL, 'update', '2019-07-02 22:48:05', '2019-07-13 03:52:00');

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `id` int(10) UNSIGNED NOT NULL,
  `team_id` int(11) NOT NULL,
  `skills_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skills_percentage` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `team_id`, `skills_name`, `skills_percentage`, `created_at`, `updated_at`) VALUES
(21, 6, 'Laravel', 88, NULL, NULL),
(22, 6, 'HTML', 87, NULL, NULL),
(23, 6, 'CSS', 98, NULL, NULL),
(26, 7, 'dsfds', 37, NULL, NULL),
(27, 3, 'HTML', 95, NULL, NULL),
(28, 3, 'CSS', 90, NULL, NULL),
(29, 3, 'JavaScript', 75, NULL, NULL),
(30, 3, 'JQuery', 75, NULL, NULL),
(31, 3, 'Vue Js', 60, NULL, NULL),
(32, 3, 'WordPress', 80, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `slug`, `title`, `subtitle`, `image`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'who-we-are', 'Who We Are?', 'We offer website as well as web application development on WordPress, Woocommerce,  Laravel, JavaScript, & PHP that are tailor made to target your customers and make your presence live on the web.Owing to the flexibility and elasticity of cloud-based offerings, we also ensure your infrastructure scales according to your actual requirement, instead of overpaying from the get go and needlessly sinking capital.', '1562150353.jpg', 'active', 1, '2019-07-02 03:03:56', '2019-07-03 04:39:14'),
(3, 'we-made-our-software-100-errorless', 'Web Maintenance', 'We have various packages giving you peace of mind. This includes help desk assistance and updating when necessary. We can also provide templates or Content Management software which allows you to manage your own website.', '1562150489.png', 'active', 3, '2019-07-02 03:13:54', '2019-07-03 04:41:29'),
(4, 'support', 'Support', 'We works with you to transform your high level “ideas” into full working sites. Our sites vary in complexity – so we understand the need to provide support to all our clients as and when needed.', '1562150665.jpg', 'active', 2, '2019-07-03 04:44:25', '2019-07-03 04:44:25');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` longtext COLLATE utf8mb4_unicode_ci,
  `avatar` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag_line` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `important_skills` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_link` text COLLATE utf8mb4_unicode_ci,
  `githtb_link` text COLLATE utf8mb4_unicode_ci,
  `stackoverflow_link` text COLLATE utf8mb4_unicode_ci,
  `linkedin_link` text COLLATE utf8mb4_unicode_ci,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `slug`, `name`, `email`, `phone`, `designation`, `about`, `avatar`, `tag_line`, `important_skills`, `facebook_link`, `githtb_link`, `stackoverflow_link`, `linkedin_link`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'atik-hasmee', 'Atik Hasmee', 'atik@gmail.com', '1265656565', 'Software Engineer', '<p>adsf adsf adsf afd asdf adfjkadskjfh asdjfh akdshf jahieuhf adhajsdf jadsfa jasdhfjads</p><p>adsf kajdsf  asjdfh aksdjfh adf akjsdfhh jaksdhfaksdjf aksjdfh akdjshfkasdfh kjasdh fasjdhfa</p><p>asdkjf akjsdf kjadhf jahsdfjashdfjasdhfjasdhfasd</p>', '1562055587.jpg', NULL, NULL, 'https://www.facebook.com/', 'https://github.com/', 'https://stackoverflow.com/', NULL, 'active', 7, '2019-07-02 01:36:35', '2019-07-03 03:53:19'),
(3, 'md-shahinur-rahman', 'Md. Shahinur Rahman', 'coredeveloper.2013@gmail.com', '+8801715712931', 'Founder, Backend Developer', '<p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">Nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.<br></p><p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">Nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.<br></p><p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '1562386371.jpg', 'The Guardian', 'Lorem ipsum dolor', 'https://www.facebook.com/', 'https://github.com/', 'https://stackoverflow.com/', 'https://www.linkedin.com/', 'active', 1, '2019-07-05 22:12:51', '2019-07-27 01:34:25'),
(4, 'sudip-palash', 'Sudip Palash', 'SudipPalash@gmail.com', '+8801712960833', 'Sr. Software Engineer', '<p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p><p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.</p><p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '1562386573.png', 'One Man Army', NULL, 'https://www.facebook.com/', 'https://github.com/', 'https://stackoverflow.com/', 'https://www.linkedin.com/', 'active', 3, '2019-07-05 22:16:13', '2019-07-17 05:37:19'),
(5, 'ridwanul-hafiz', 'Ridwanul Hafiz', 'RidwanulHafiz@gmail.com', '+8801717863320', 'Sr. Software Engineer', '<p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">Nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.<br></p><p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">Nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.<br></p><p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">Nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.<br></p><p style=\"color: rgb(80, 102, 143); font-family: Montserrat, sans-serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '1562386710.jpg', 'One Man Army', NULL, 'https://www.facebook.com/', 'https://github.com/', 'https://stackoverflow.com/', 'https://www.linkedin.com/', 'active', 2, '2019-07-05 22:18:30', '2019-07-17 05:37:32'),
(6, 'rony', 'Rony', 'rony@gmail.com', '+8801715712931', 'Software Engineer', '<p>adsf asdf asd</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'active', 4, '2019-07-11 03:30:27', '2019-07-12 04:22:08'),
(8, 'rony-1', 'Rony', 'faizanali2012a@gmail.com', '+8801712960833', 'Software Engineer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'inactive', 6, '2019-07-11 04:00:10', '2019-07-11 04:00:10');

-- --------------------------------------------------------

--
-- Table structure for table `technologies`
--

CREATE TABLE `technologies` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `technologies`
--

INSERT INTO `technologies` (`id`, `title`, `image`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(4, 'HTML', '1563959923.png', 'active', 1, '2019-07-18 21:58:43', '2019-07-24 03:18:43'),
(5, 'CSS', '1563960989.png', 'active', 2, '2019-07-18 21:59:21', '2019-07-24 03:36:29'),
(6, 'JavaScript', '1563960310.png', 'active', 4, '2019-07-18 21:59:41', '2019-07-24 03:25:10'),
(7, 'Jquery', '1563961471.png', 'active', 5, '2019-07-18 22:00:07', '2019-07-24 03:44:31'),
(8, 'Vue Js', '1563960580.png', 'active', 7, '2019-07-18 22:00:27', '2019-07-24 03:29:40'),
(9, 'React', '1563960829.png', 'active', 9, '2019-07-18 22:01:03', '2019-07-24 03:33:49'),
(10, 'Angular', '1563961160.png', 'active', 11, '2019-07-18 22:01:16', '2019-07-24 03:39:20'),
(11, 'Node', '1563961075.png', 'active', 12, '2019-07-18 22:01:30', '2019-07-24 03:37:55'),
(12, 'Laravel', '1563960871.png', 'active', 10, '2019-07-18 22:01:54', '2019-07-24 03:34:31'),
(13, 'Less', '1563960178.png', 'active', 3, '2019-07-24 03:22:58', '2019-07-24 03:22:58'),
(14, 'Ajax', '1563960771.png', 'active', 6, '2019-07-24 03:26:09', '2019-07-24 03:32:51'),
(15, 'Vuetify', '1563960659.png', 'active', 8, '2019-07-24 03:30:59', '2019-07-24 03:30:59');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `client_name`, `company_name`, `comments`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 'MATT RYAN', 'CAP WEB SOLUTIONS', 'I\'ve recently purchased 3 different WSM themes on behalf of clients - Colin, Joshua and Bradley. The themes have come together quite nicely, but best of all is the tech support I\'ve received through the forums. Chris Cree is a remarkable resource and walked me through some tricky customizations. Kudos to WSM.', '1564228133.png', 'active', '2019-07-27 05:48:53', '2019-07-27 05:48:53'),
(3, 'DENISE TRAVER', 'Website', 'This was my first use of a Web Savvy Marketing theme, and I have to say that I am so thoroughly impressed with the beauty, ease of editing, and the squeaky clean and fast code. Chris Cree was exceptionally insightful and was able to tell me exactly what to change to get it working perfectly. I am now looking at changing/updating several sites to Web Savvy\'s themes. I am hooked! Thank you.', '1564228175.png', 'active', '2019-07-27 05:49:35', '2019-07-27 05:49:35'),
(4, 'ANNETTE LISKEY', 'JAMES MADISON UNIVERSITY', 'I really appreciate everything you and all of Web Savvy Marketing have done. This theme is very easy to work with and everyone who’s seen it loves the design.', '1564228216.png', 'active', '2019-07-27 05:50:16', '2019-07-27 05:50:16'),
(5, 'SANDY HENDERSON', 'RUG RATS', 'It’s rare that you come across standout talent like Rebecca Gill!. I learned more in one hour of coaching, than 3 years of paying for marketing services from professional companies. Web Savvy Marketing comes highly recommended!', '1564228248.png', 'active', '2019-07-27 05:50:49', '2019-07-27 05:50:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` enum('admin','editor') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'editor',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','deactivate') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'deactivate',
  `sort` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `email`, `email_verified_at`, `about`, `phone`, `image`, `password`, `status`, `sort`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Rashiqul Rony', 'rony@gmail.com', NULL, NULL, '+8801738030343', '1562307184.jpg', '$2y$10$2YpJvQuvGkvctDUdDqqmNOpcdGEMs8mAXxcDXWwIXnvscmtG6z0Xq', 'active', 1, 'zEyslYNQYco7KUS8Hye4WurXH6zvlBmPT818bVizLzfk4NSIWGxI6ToOUeTt', '2019-06-30 22:25:49', '2019-07-05 00:13:04'),
(3, 'editor', 'Xuher Anzum', 'rony1@gmail.com', NULL, NULL, NULL, '1562308206.jpg', '$2y$10$Hrwx293R402VQcjhRJGoCeXYpEW90zFqOPUzk9SlQHunOj5ZT1niC', 'deactivate', 2, 'XE6ZD7aTPvHz40vEt9WD42DI3FNX8pARHA1dOvF7Wlkt7lIBx9mH7FaZv4TA', '2019-07-05 00:30:06', '2019-07-05 00:50:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `helps`
--
ALTER TABLE `helps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `informative_texts`
--
ALTER TABLE `informative_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter_subscriptions`
--
ALTER TABLE `newsletter_subscriptions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `newsletter_subscriptions_email_unique` (`email`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `portfolios`
--
ALTER TABLE `portfolios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio_category_relations`
--
ALTER TABLE `portfolio_category_relations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio_features`
--
ALTER TABLE `portfolio_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio_images`
--
ALTER TABLE `portfolio_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio_technology_relations`
--
ALTER TABLE `portfolio_technology_relations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_industries`
--
ALTER TABLE `service_industries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_offered`
--
ALTER TABLE `service_offered`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_technology_relations`
--
ALTER TABLE `service_technology_relations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `technologies`
--
ALTER TABLE `technologies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `helps`
--
ALTER TABLE `helps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `informative_texts`
--
ALTER TABLE `informative_texts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `newsletter_subscriptions`
--
ALTER TABLE `newsletter_subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `portfolios`
--
ALTER TABLE `portfolios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `portfolio_category_relations`
--
ALTER TABLE `portfolio_category_relations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT for table `portfolio_features`
--
ALTER TABLE `portfolio_features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=353;

--
-- AUTO_INCREMENT for table `portfolio_images`
--
ALTER TABLE `portfolio_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `portfolio_technology_relations`
--
ALTER TABLE `portfolio_technology_relations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `service_industries`
--
ALTER TABLE `service_industries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `service_offered`
--
ALTER TABLE `service_offered`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `service_technology_relations`
--
ALTER TABLE `service_technology_relations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `technologies`
--
ALTER TABLE `technologies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
