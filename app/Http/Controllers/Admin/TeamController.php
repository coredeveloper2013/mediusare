<?php

namespace App\Http\Controllers\Admin;

use App\Models\Skill;
use App\Models\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Image;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::orderBy('sort', 'ASC')->get();
        return view('admin.teams', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.team_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|max:191',
            'skills_name.*' => 'required|max:50',
//            'skills_percentage' => 'required|integer|between:30,99',
//            'skills_percentage.*' => 'required|integer|between:30,99',
            'email' => 'required|email|max:191',
            'phone' => 'required|max:16',
            'designation' => 'required|max:191',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $team  = new Team();
        if($request->hasFile('avatar')) {
            $image = $request->file('avatar');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/team/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(300, 300)->save($imageUrl);
            $team->avatar = $fileName;
        }
        $team->name                = $request->name;
        $team->email               = $request->email;
        $team->phone               = $request->phone;
        $team->designation         = $request->designation;
        $team->about               = $request->about;
        $team->tag_line            = $request->tag_line;
        $team->important_skills    = $request->important_skills;
        $team->facebook_link       = $request->facebook_link;
        $team->githtb_link         = $request->githtb_link;
        $team->stackoverflow_link  = $request->stackoverflow_link;
        $team->linkedin_link       = $request->linkedin_link;
        $team->status              = $request->status;
        if ($team->save()){
            if (!empty($data['skills_name'])) {
                $skillData = [];
                foreach ($data['skills_name'] as $k => $name) {
                    $skillData[] = [
                        'team_id' => $team->id,
                        'skills_name' => $name,
                        'skills_percentage' => $data['skills_percentage'][$k]
                    ];
                }
                if (!empty($skillData)) {
                    Skill::insert($skillData);
                }
            }
        }
        return redirect()->back()->with('success', 'Team save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = Team::find($id);
        return view('admin.team_edit', compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|max:191',
            'email' => 'required|email|max:191',
            'phone' => 'required|max:16',
            'designation' => 'required|max:191',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $team  = Team::find($id);

        if($request->hasFile('avatar')) {
            if ($team->avatar){
                if (!file_exists(public_path('media/team/'.$team->avatar))){
                    $team->avatar = null;
                }else{
                    unlink('media/team/'. $team->avatar);
                }
            }
            $image = $request->file('avatar');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/team/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(300, 300)->save($imageUrl);
            $team->avatar = $fileName;
        }
        $team->name              = $request->name;
        $team->email              = $request->email;
        $team->phone              = $request->phone;
        $team->designation        = $request->designation;
        $team->about              = $request->about;
        $team->tag_line           = $request->tag_line;
        $team->important_skills   = $request->important_skills;
        $team->facebook_link      = $request->facebook_link;
        $team->githtb_link        = $request->githtb_link;
        $team->stackoverflow_link = $request->stackoverflow_link;
        $team->linkedin_link      = $request->linkedin_link;
        $team->status             = $request->status;
        if ($team->save()){
            Skill::where('team_id', $id)->delete();

            if (!empty($data['skills_name'])) {
                //array filter for zero empty value check
                $skillName = $data['skills_name'];
                $skillName = !empty($skillName) ? array_values(array_filter($skillName)) : array();
                $skillsPercentage = $data['skills_percentage'];
                $skillsPercentage = !empty($skillsPercentage) ? array_values(array_filter($skillsPercentage)) : array();

                $skillData = [];
                foreach ($skillName as $k => $name) {
                    $skillData[] = [
                        'team_id' => $team->id,
                        'skills_name' => $name,
                        'skills_percentage' => $skillsPercentage[$k]
                    ];
                }
                if (!empty($skillData)) {
                    Skill::insert($skillData);
                }
            }
        }


        return redirect(route('team.index'))->with('success', 'Team update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = Team::find($id);
        if ($team->avatar){
            if (!file_exists(public_path('media/team/'.$team->avatar))){
                $team->avatar = null;
            }else{
                unlink('media/team/'. $team->avatar);
            }
        }
        $team->delete();
        return redirect()->back()->with('error', 'Team deleted!!');
    }

    public function sortable(Request $request){
        $teams = Team::all();
        foreach ($teams as $team) {
            $team->timestamps = false; // To disable update_at field updation
            $id = $team->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $team->update(['sort' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }
}
