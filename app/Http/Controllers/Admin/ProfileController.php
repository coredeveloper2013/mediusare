<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Validator;
use Image;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('admin.profile.index', compact('user'));
    }

    public function updateProfile(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|max:191',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $id = Auth::id();
        $user = User::find($id);

        if ($request->hasFile('image')) {
            if (!file_exists(public_path('media/user/' . $user->image))) {
                $user->image = null;
            } else {
                unlink('media/user/' . $user->image);
            }
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/user/';
            $imageUrl = $directory . $fileName;
            Image::make($image)->resize(300, 300)->save($imageUrl);
            $user->image = $fileName;
        }
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->about = $request->about;
        $user->save();
        return redirect(route('profile.index'))->with('success', 'Profile update successfully');
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'current_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        $hashedPassword = Auth::user()->password;
        if (Hash::check($request->current_password, $hashedPassword)) {
            if (!Hash::check($request->password, $hashedPassword)) {
                $user = User::find(Auth::id());
                $user->password = Hash::make($request->password);
                $user->save();
                Auth::logout();
                return redirect()->route('login')->with('success', 'Password Successfully Changed!');
            } else {
                return redirect()->back()->with('error', 'New password cannot be the same as old password.');
            }
        } else {
            return redirect()->back()->with('error', 'Current password not match.');
        }
    }

}
