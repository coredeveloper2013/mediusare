<?php

namespace App\Http\Controllers\Admin;

use App\Models\Career;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Image;

class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $careers = Career::orderBy('id', 'desc')->get();
        return view('admin.career', compact('careers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.career_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:255',
            'vacancy' => 'required|numeric',
            'deadline' => 'required',
            'job_context' => 'required',
            'job_responsibilities' => 'required',
            'employment_status' => 'required',
            'experience_requirements' => 'required',
            'educational_requirements' => 'required',
            'additional_requirements' => 'required',
            'salary' => 'required',
            'apply_email' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/career/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(920, 530)->save($imageUrl);
            $data['image'] = $fileName;
        }
        Career::create($data);
        return redirect()->back()->with('success', 'Career save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $career = Career::find($id);
        return  view('admin.career_view', compact('career'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $career = Career::find($id);
        return  view('admin.career_edit', compact('career'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:255',
            'vacancy' => 'required|numeric',
            'deadline' => 'required',
            'job_context' => 'required',
            'job_responsibilities' => 'required',
            'employment_status' => 'required',
            'experience_requirements' => 'required',
            'educational_requirements' => 'required',
            'additional_requirements' => 'required',
            'salary' => 'required',
            'apply_email' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        if($request->hasFile('image')) {
            $career = Career::find($id);

            if ($career->image){
                if (!file_exists(public_path('media/career/'.$career->image))){
                    $career->image = null;
                }else{
                    unlink('media/career/'. $career->image);
                }
            }

            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/career/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(920, 530)->save($imageUrl);
            $data['image'] = $fileName;
        }
        Career::find($id)->update($data);
        return redirect()->back()->with('success', 'Career update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $career = Career::find($id);

        if ($career->image){
            if (!file_exists(public_path('media/career/'.$career->image))){
                $career->image = null;
            }else{
                unlink('media/career/'. $career->image);
            }
        }
        $career->delete();
        return redirect()->back()->with('success', 'Career delete successfully');
    }
}
