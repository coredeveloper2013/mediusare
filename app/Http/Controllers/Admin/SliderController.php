<?php

namespace App\Http\Controllers\Admin;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Image;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::orderBy('sort', 'ASC')->get();
        return view('admin.sliders', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:191',
            'subtitle' => 'required',
            'image' => 'required|image',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $slider  = new Slider();
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/slider/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(1920, 880)->save($imageUrl);
            $slider->image = $fileName;
        }
        $slider->title    = $request->title;
        $slider->subtitle = $request->subtitle;
        $slider->status   = $request->status;
        $slider->save();
        return redirect()->back()->with('success', 'Slider save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('admin.slider_edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:191',
            'subtitle' => 'required',
//            'image' => 'required|image',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $slider = Slider::find($id);
        if($request->hasFile('image')) {
            if (!file_exists(public_path('media/slider/'.$slider->image))){
                $slider->image = null;
            }else{
                unlink('media/slider/'. $slider->image);
            }
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/slider/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(1920, 880)->save($imageUrl);
            $slider->image = $fileName;
        }
        $slider->title    = $request->title;
        $slider->subtitle = $request->subtitle;
        $slider->status   = $request->status;
        $slider->save();
        return redirect(route('slider.index'))->with('success', 'Slider update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::find($id);
        if ($slider->image){
            if (!file_exists(public_path('media/slider/'.$slider->image))){
                $slider->image = null;
            }else{
                unlink('media/slider/'. $slider->image);
            }
        }
        $slider->delete();
        return redirect()->back()->with('error', 'Slider deleted!!');
    }


    public function sortable(Request $request){
        $sliders = Slider::all();
        foreach ($sliders as $slider) {
            $slider->timestamps = false; // To disable update_at field updation
            $id = $slider->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $slider->update(['sort' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }
}
