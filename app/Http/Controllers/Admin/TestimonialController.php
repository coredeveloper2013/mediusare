<?php

namespace App\Http\Controllers\Admin;

use App\Models\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Image;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = Testimonial::get();
        return view('admin.testimonials', compact('testimonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.testimonial_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'client_name' => 'required|max:191',
            'company_name' => 'required',
            'image' => 'required|image',
            'comments' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $testimonial  = new Testimonial();
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/testimonial/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(300, 300)->save($imageUrl);
            $testimonial->image = $fileName;
        }
        $testimonial->client_name    = $request->client_name;
        $testimonial->company_name   = $request->company_name;
        $testimonial->comments       = $request->comments;
        $testimonial->status         = $request->status;
        $testimonial->save();
        return redirect()->back()->with('success', 'Testimonial save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $testimonial = Testimonial::find($id);
        return view('admin.testimonial_edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'client_name' => 'required|max:191',
            'company_name' => 'required',
//            'image' => 'required|image',
            'comments' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $testimonial = Testimonial::find($id);
        if($request->hasFile('image')) {
            if ($testimonial->image){
                if (!file_exists(public_path('media/testimonial/'.$testimonial->image))){
                    $testimonial->image = null;
                }else{
                    unlink('media/testimonial/'. $testimonial->image);
                }
            }
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/testimonial/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(300, 300)->save($imageUrl);
            $testimonial->image = $fileName;
        }
        $testimonial->client_name    = $request->client_name;
        $testimonial->company_name   = $request->company_name;
        $testimonial->comments       = $request->comments;
        $testimonial->status         = $request->status;
        $testimonial->save();
        return redirect(route('testimonial.index'))->with('success', 'Testimonial update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial = Testimonial::find($id);
        if ($testimonial->image){
            if (!file_exists(public_path('media/testimonial/'.$testimonial->image))){
                $testimonial->image = null;
            }else{
                unlink('media/testimonial/'. $testimonial->image);
            }
        }
        $testimonial->delete();
        return redirect()->back()->with('success', 'Testimonial update successfully');
    }
}
