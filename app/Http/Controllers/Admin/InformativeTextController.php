<?php

namespace App\Http\Controllers\Admin;

use App\Models\InformativeText;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class InformativeTextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informativeTexts = InformativeText::get();
        return view('admin.informative_text', compact('informativeTexts') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.informative_text_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:191',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $informativeText  = new InformativeText();
        $informativeText->title   = $request->title;
        $informativeText->status  = $request->status;
        $informativeText->save();
        return redirect()->back()->with('success', 'Informative Text save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $informativeText  = InformativeText::find($id);
        return view('admin.informative_text_edit', compact('informativeText'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:191',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $informativeText  = InformativeText::find($id);
        $informativeText->title   = $request->title;
        $informativeText->status  = $request->status;
        $informativeText->save();
        return redirect(route('informative-text.index'))->with('success', 'Informative Text update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        InformativeText::find($id)->delete();
        return redirect(route('informative-text.index'))->with('success', 'Informative Text delete successfully');
    }
}
