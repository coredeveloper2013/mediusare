<?php

namespace App\Http\Controllers\Admin;

use App\Models\About;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use Validator;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abouts = About::orderBy('sort', 'ASC')->get();
        return view('admin.abouts', compact('abouts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.about_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:191',
            'description' => 'required',
            'image' => 'required|image',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $about  = new About();
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/about/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(500, 600)->save($imageUrl);
            $about->image = $fileName;
        }
        $about->title    = $request->title;
        $about->description = $request->description;
        $about->status   = $request->status;
        $about->save();
        return redirect()->back()->with('success', 'About save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about = About::find($id);
        return view('admin.about_edit', compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:191',
            'description' => 'required',
//            'image' => 'required|image',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $about  = About::find($id);

        if($request->hasFile('image')) {
            if ($about->iamge){
                if (!file_exists(public_path('media/about/'.$about->iamge))){
                    $about->iamge = null;
                }else{
                    unlink('media/about/'. $about->iamge);
                }
            }

            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/about/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(500, 600)->save($imageUrl);
            $about->image = $fileName;
        }
        $about->title    = $request->title;
        $about->description = $request->description;
        $about->status   = $request->status;
        $about->save();
        return redirect(route('about.index'))->with('success', 'About update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $about  = About::find($id);
        if ($about->iamge){
            if (!file_exists(public_path('media/about/'.$about->iamge))){
                $about->iamge = null;
            }else{
                unlink('media/about/'. $about->iamge);
            }
        }
        $about->delete();
        return redirect(route('about.index'))->with('success', 'Delete update successfully');
    }


    public function sortable(Request $request){
        $abouts = About::all();
        foreach ($abouts as $about) {
            $about->timestamps = false; // To disable update_at field updation
            $id = $about->id;
            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $about->update(['sort' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }
}
