<?php

namespace App\Http\Controllers\Admin;

use App\Models\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Image;
use Validator;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partner::get();
        return view('admin.partners', compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partner_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|max:191',
            'country' => 'required|max:191',
            'image' => 'required|image',
            'about' => 'required',
            'status' => Rule::in(['active', 'deactivate']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $partner  = new Partner();

        if($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/partner/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(300, 300)->save($imageUrl);
            $partner->image = $fileName;
        }

        $partner->name      = $request->name;
        $partner->country   = $request->country;
        $partner->about     = $request->about;
        $partner->status    = $request->status;
        $partner->save();
        return redirect(route('partner.index'))->with('success', 'Partner save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partner = Partner::find($id);
        return view('admin.partner_edit', compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|max:191',
            'country' => 'required|max:191',
            'about' => 'required',
            'status' => Rule::in(['active', 'deactivate']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $partner = Partner::find($id);
        if($request->hasFile('image')) {
            if ($partner->image){
                if (!file_exists(public_path('media/partner/'.$partner->image))){
                    $partner->image = null;
                }else{
                    unlink('media/partner/'. $partner->image);
                }
            }
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/partner/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(300, 300)->save($imageUrl);
            $partner->image = $fileName;
        }

        $partner->name      = $request->name;
        $partner->country   = $request->country;
        $partner->about     = $request->about;
        $partner->status    = $request->status;
        $partner->save();
        return redirect(route('partner.index'))->with('success', 'Partner update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner = Partner::find($id);
        if ($partner->image){
            if (!file_exists(public_path('media/partner/'.$partner->image))){
                $partner->image = null;
            }else{
                unlink('media/partner/'. $partner->image);
            }
        }
        $partner->delete();
        return redirect(route('partner.index'))->with('success', 'Partner delete successfully');
    }
}
