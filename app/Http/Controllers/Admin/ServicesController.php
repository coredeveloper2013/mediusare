<?php

namespace App\Http\Controllers\Admin;

use App\Models\Service;
use App\Models\ServiceIndustries;
use App\Models\ServiceOffered;
use App\Models\ServiceTechnologyRelations;
use App\Models\Technology;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Image;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::orderBy('sort', 'ASC')->get();
        return view('admin.services', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $technologies = Technology::get();
        return view('admin.service_create', compact('technologies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:255',
//            'description' => 'required',
            'status' => 'required',
            'icon' => 'required|max:32',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $service = new Service();
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/service/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(1903, 600)->save($imageUrl);
            $service->image = $fileName;
        }

        if ($request->hasFile('industries_image')) {
            $file = $request->file('industries_image');
            $name = time() . '.' . $file->getClientOriginalExtension();
            $file->move('media/industries/', $name);
            $service->industries_image = $name;
        }

        $service->title              = $request->title;
        $service->short_description  = $request->short_description;
        $service->description_left   = $request->description_left;
        $service->description_right  = $request->description_right;
        $service->date               = $request->date;
        $service->icon               = $request->icon;
        $service->status             = $request->status;

        if ($service->save()){
            if (!empty($data['offered_services_title'])) {
                //array filter for zero empty value check
                $offered_services_title = $data['offered_services_title'];
                $offered_services_title = !empty($offered_services_title) ? array_values(array_filter($offered_services_title)) : array();
                $offered_services_data = [];
                foreach ($offered_services_title as $k => $offered_title) {
                    $offered_services_data[] = [
                        'service_id' => $service->id,
                        'offered_services_title' => $offered_title,
                    ];
                }
                if (!empty($offered_services_data)) {
                    ServiceOffered::insert($offered_services_data);
                }
            }

            if (!empty($data['industries_title'])) {
                //array filter for zero empty value check
                $industries_title = $data['industries_title'];
                $industries_title = !empty($industries_title) ? array_values(array_filter($industries_title)) : array();
                $industries_data = [];
                foreach ($industries_title as $k => $title) {
                    $industries_data[] = [
                        'service_id' => $service->id,
                        'services_industries_title' => $title,
                    ];
                }
                if (!empty($industries_data)) {
                    ServiceIndustries::insert($industries_data);
                }
            }

            if (!empty($data['technology'])) {
                $technologyData = [];
                foreach ($data['technology'] as $k => $technologyId) {
                    $technologyData[] = [
                        'service_id' => $service->id,
                        'technology_id' => $technologyId,
                    ];
                }
                if (!empty($technologyData)) {
                    ServiceTechnologyRelations::insert($technologyData);
                }
            }
        }
        return redirect()->back()->with('success', 'Service save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::find($id);
        $service->getTechnologies = $service->getTechnologies->pluck('technology_id')->toArray();
        $technologies = Technology::get();
        return view('admin.service_edit', compact('service', 'technologies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|max:255',
//            'description' => 'required',
            'status' => 'required',
            'icon' => 'required|max:32',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $service = Service::find($id);

        if($request->hasFile('image')) {
            if (!file_exists(public_path('media/service/'.$service->image))){
                $service->image = null;
            }else{
                unlink('media/service/'. $service->image);
            }
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/service/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(1903, 600)->save($imageUrl);
            $service->image = $fileName;
        }

        if ($request->hasFile('industries_image')) {
            if ($service->industries_image){
                unlink('media/industries/'. $service->industries_image);
            }
            $file = $request->file('industries_image');
            $name = time() . '.' . $file->getClientOriginalExtension();
            $file->move('media/industries/', $name);
            $service->industries_image = $name;
        }

        $service->title              = $request->title;
        $service->short_description  = $request->short_description;
        $service->description_left   = $request->description_left;
        $service->description_right  = $request->description_right;
        $service->date               = $request->date;
        $service->icon               = $request->icon;
        $service->status             = $request->status;
        if ($service->save()){
            if (!empty($data['offered_services_title'])) {
                ServiceOffered::where('service_id', $id)->delete();
                //array filter for zero empty value check
                $offered_services_title = $data['offered_services_title'];
                $offered_services_title = !empty($offered_services_title) ? array_values(array_filter($offered_services_title)) : array();
                $offered_services_data = [];
                foreach ($offered_services_title as $k => $offered_title) {
                    $offered_services_data[] = [
                        'service_id' => $service->id,
                        'offered_services_title' => $offered_title,
                    ];
                }
                if (!empty($offered_services_data)) {
                    ServiceOffered::insert($offered_services_data);
                }
            }

            if (!empty($data['industries_title'])) {
                ServiceIndustries::where('service_id', $id)->delete();
                //array filter for zero empty value check
                $industries_title = $data['industries_title'];
                $industries_title = !empty($industries_title) ? array_values(array_filter($industries_title)) : array();
                $industries_data = [];
                foreach ($industries_title as $k => $title) {
                    $industries_data[] = [
                        'service_id' => $service->id,
                        'services_industries_title' => $title,
                    ];
                }
                if (!empty($industries_data)) {
                    ServiceIndustries::insert($industries_data);
                }
            }

            if (!empty($data['technology'])) {
                ServiceTechnologyRelations::where('service_id', $id)->delete();
                $technologyData = [];
                foreach ($data['technology'] as $k => $technologyId) {
                    $technologyData[] = [
                        'service_id' => $service->id,
                        'technology_id' => $technologyId,
                    ];
                }
                if (!empty($technologyData)) {
                    ServiceTechnologyRelations::insert($technologyData);
                }
            }
        }

        return redirect(route('services.index'))->with('success', 'Service update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);

        if ($service->image){
            if (!file_exists(public_path('media/service/'.$service->image))){
                $service->image = null;
            }else{
                unlink('media/service/'. $service->image);
            }
        }

        if ($service->industries_image){
            if (!file_exists(public_path('media/industries/'.$service->industries_image))){
                $service->industries_image = null;
            }else{
                unlink('media/industries/'. $service->industries_image);
            }
        }

        $service->delete();
        return redirect()->back()->with('error', 'Service deleted');
    }

    public function sortable(Request $request){
        $services = Service::all();
        foreach ($services as $service) {
            $service->timestamps = false; // To disable update_at field updation
            $id = $service->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $service->update(['sort' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }


}
