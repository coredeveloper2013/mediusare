<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Career;
use App\Models\Category;
use App\Models\Client;
use App\Models\Contact;
use App\Models\Feature;
use App\Models\InformativeText;
use App\Models\NewsletterSubscription;
use App\Models\Partner;
use App\Models\Portfolio;
use App\Models\Service;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\Team;
use App\Models\Technology;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Mail;
use Validator;

class WebsiteController extends Controller
{
    public function index()
    {
        return view('website.index');
    }

    public function home()
    {
        return view('vue.layout');
    }

    // For Portfolios
    public function getPortfolios($slug = null)
    {
//        $portfolios = Portfolio::where('status', 'active')->orderBy('sort', 'ASC')->get();
//        $portfolios = DB::table('portfolios')
//            ->join('portfolio_category_relations as re_category', 'portfolios.id', '=', 're_category.portfolio_id')
//            ->leftJoin('categories', 'categories.id', '=', 're_category.category_id')
//            ->select('portfolios.*', 'categories.title as category_title', 'categories.slug as category_slug')
//            ->where('portfolios.status', 'active')
//            ->orderBy('portfolios.sort', 'ASC')
//            ->paginate(16);


        $sql = Portfolio::where('portfolios.status', 'active')->orderBy('portfolios.sort', 'ASC');
        if ($slug) {
            $sql->join('portfolio_category_relations as re_category', 'portfolios.id', '=', 're_category.portfolio_id');
            $sql->join('categories', 'categories.id', '=', 're_category.category_id');
            $sql->select('portfolios.*', 'categories.title as category_title');
            $sql->where('categories.slug', $slug);
        }
        $portfolios = $sql->paginate(15);

        return response()->json($portfolios);
    }

    public function getPortfolioShowcase()
    {
        $portfolios = Portfolio::where('status', 'active')
            ->with('getCategories')
            ->where('showcase', 1)
            ->orderBy('sort', 'ASC')
            ->paginate(10);
        return response()->json($portfolios);
    }

    public function getCategoryByPortfolio($slug)
    {
        $category = Category::where('slug', $slug)->with('getCategories.portfolios')->first();
        return response()->json($category);
    }

    public function getPortfolioDetails($slug)
    {
        $portfolio = Portfolio::where('slug', $slug)
            ->with('getCategories', 'getTechnologies.technology', 'getFeatures', 'getImages')
            ->first();
        return response()->json([
            'message' => 'success',
            'obj' => $portfolio,
            'categories' => isset($portfolio->category->title) ? $portfolio->category->title : '',
            'date' => date('F d, Y', strtotime($portfolio->date)),
            'image' => asset('media/portfolio/' . $portfolio->image),
        ]);
    }

    //For Sliders
    public function getSliders()
    {
        $sliders = Slider::where('status', 'active')->orderBy('sort', 'ASC')->get();
        return response()->json($sliders);
    }

    //For informative text
    public function getInformativeText()
    {
        $informativeText = InformativeText::where('status', 'active')->pluck('title');
        return response()->json($informativeText);
    }

    // For Categories
    public function getCategories()
    {
        $categories = Category::where('status', 'active')->get();
        return response()->json($categories);
    }

    // For Features
    public function getFeatures()
    {
        $features = Feature::where('status', 'active')->orderBy('id', 'DESC')->get();
        return response()->json($features);
    }

    // For Technology
    public function getTechnology()
    {
        $technologies = Technology::where('status', 'active')->select('title', 'image')->orderBy('sort', 'ASC')->get();
        return response()->json($technologies);
    }

    // For Clients
    public function getClients()
    {
        $clients = Client::where('status', 'active')
            ->select('title', 'image')
            ->orderBy('sort', 'ASC')->get();
        return response()->json($clients);
    }

    // For Abouts
    public function getAbouts()
    {
        $abouts = About::where('status', 'active')->orderBy('sort', 'ASC')->get();
        return response()->json($abouts);
    }

    public function getFeatureDetails($slug)
    {
        $feature = Feature::where('slug', $slug)->first();
        return response()->json([
            'message' => 'success',
            'obj' => $feature,
            'date' => date('F d, Y', strtotime($feature->created_at)),
            'image' => asset('media/feature/' . $feature->image),
        ]);
    }

    // For Services
    public function getServices()
    {
        $services = Service::where('status', 'active')
            ->select('slug', 'title', 'short_description', 'icon')
            ->orderBy('sort', 'ASC')->get();
        return response()->json($services);
    }

    // For testimonials
    public function getTestimonials()
    {
        $testimonials = Testimonial::where('status', 'active')
            ->select('comments', 'image', 'client_name', 'company_name')
            ->orderBy('id', 'DESC')
            ->get();
        return response()->json($testimonials);
    }

    // For testimonials
    public function getPartners()
    {
        $partners = Partner::where('status', 'active')->orderBy('id', 'DESC')->get();
        return response()->json($partners);
    }

    public function getServiceDetails($slug)
    {
        $service = Service::where('slug', $slug)
            ->with('getTechnologies.technology', 'getServiceOffered', 'getServiceIndustries')
            ->first();
        return response()->json([
            'message' => 'success',
            'obj' => $service,
            'image' => asset('media/service/' . $service->image),
            'industries_image' => asset('media/industries/' . $service->industries_image),
            'date' => date('F Y', strtotime($service->date)),
        ]);
    }

    // For Team
    public function getTeam()
    {
        $teams = Team::where('status', 'active')->select('slug', 'avatar', 'name', 'designation', 'important_skills', 'tag_line')->with('skills')->orderBy('sort', 'ASC')->get();
        foreach ($teams as $k => $tm) {
            if ($tm->avatar == '' || !file_exists(public_path('media/team/' . $tm->avatar))) {
                $teams[$k]->avatar = null;
            }
        }
        return response()->json($teams);
    }

    public function getTeamDetails($slug)
    {
        $team = Team::with('skills')->where('slug', $slug)->first();
        return response()->json([
            'message' => 'success',
            'obj' => $team,
            'image' => asset('media/team/' . $team->avatar),
        ]);
    }


    // For Career
    public function getCareers()
    {
        $careers = Career::where('status', 'active')->orderBy('id', 'desc')->paginate(15);;
        return response()->json($careers);
    }

    public function getCareerDetails($slug)
    {
        $career = Career::where('slug', $slug)->first();
        return response()->json([
            'message' => 'success',
            'obj' => $career,
        ]);
    }

    // For Setting
    public function getSettingInfo()
    {
//        $setting = Setting::take(1)->latest()->get();
        $setting = Setting::find(1);
        return response()->json($setting);
    }

    //Contact Store
    public function contactStore(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|max:191',
            'email' => 'required|email|max:191',
            'subject' => 'required|max:191',
            'message' => 'required|max:400',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $contact = new Contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->subject = $request->subject;
        $contact->message = $request->message;
        $contact->ip_address = $ip;
        $contact->save();

        if ($contact->save()) {
            $email = 'admin@mediusware.com';
            Mail::send('emails.contact', [
                'name' => $request->name,
                'subject' => $request->subject,
                'email' => $request->email,
                'mage' => $request->message,
                'ip' => $ip
            ],
                function ($msg) use ($email, $request) {
                    $msg->from('info@mediusware.com', 'MediusWare.Com');
                    $msg->subject($request->subject);
                    $msg->to($email);
                });
        }
        return response()->json([
            'success' => 'Thank you for contact, We will soon contact later',
            'obj' => $contact,
        ], 200);
    }

    public function teamContactStore(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|max:191',
            'email' => 'required|email|max:191',
            'subject' => 'required|max:191',
            'message' => 'required|max:400',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        if ($request->name) {
            $email = $request->teamEmail;
            Mail::send('emails.contact', [
                'name' => $request->name,
                'subject' => $request->subject,
                'email' => $request->email,
                'mage' => $request->message,
                'ip' => $ip
            ],
                function ($msg) use ($email, $request) {
                    $msg->from('info@mediusware.com', 'MediusWare.Com');
                    $msg->subject($request->subject);
                    $msg->to($email);
                });
        }
        return response()->json([
            'success' => 'Thank you for contact, We will soon contact later',
            'obj' => $request->all(),
        ], 200);
    }

//Contact Store
    public function newsletterStore(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'email' => 'required|email|unique:newsletter_subscriptions|max:191',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 5000,
                'error' => $validator->errors()
            ], 200);
        }

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $subscription = new NewsletterSubscription();
        $subscription->email = $request->email;
        $subscription->ip_address = $ip;
        $subscription->save();
//        if($subscription->save()){
//            $email = 'admin@mediusware.com';
//            Mail::send('emails.contact', [
//                'email'   => $request->email,
//                'ip'      => $ip
//            ],
//                function ($msg) use ($email, $request)
//                {
//                    $msg->from('info@mediusware.com', 'MediusWare.Com');
//                    $msg->subject('For Newsletter Subscription');
//                    $msg->to($email);
//                });
//        }
        return response()->json([
            'success' => 'Thank you for Newsletter Subscription, We will soon contact later',
            'obj' => $subscription,
        ], 200);
    }

}
