<?php

namespace App\Http\Controllers;
use File;
use Carbon\Carbon;
use App\Models\DbBackup;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class BackupMySqlController extends Controller
{
    public function sendCorn()
    {
        set_time_limit(0);

        // define target file
        $path = 'backupdb';
        File::makeDirectory(public_path($path), $mode = 0777, true, true);

        $fileName = env('DB_DATABASE').'_'.date("Y-m-d").'.sql';
        $fullPath = $path.'/'.$fileName;

        $process = new Process(sprintf(
            'mysqldump --single-transaction -u%s -p%s %s > %s',
            env('DB_USERNAME'),
            env('DB_PASSWORD'),
            env('DB_DATABASE'),
            public_path($fullPath)
        ));
        try {
            $process->mustRun();

            if ($process->isSuccessful()) {
                DbBackup::insert([
                    'export_file' => $fileName,
                    'created_at' => date("Y-m-d H:i:s"),
                ]);
            }

        } catch (ProcessFailedException $exception) {
            $this->error($exception->getMessage());
        }
    }
}