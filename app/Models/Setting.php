<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'name', 'email', 'short_name', 'address', 'phone', 'phone_2', 'phone_3', 'skype', 'map_url', 'facebook_link', 'twitter_link', 'githtb_link', 'stackoverflow_link', 'linkedin_link', 'status',
        'home_title', 'home_details', 'pinterest_link', 'youtube_link', 'youtube_link'
    ];
}
