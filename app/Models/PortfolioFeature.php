<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioFeature extends Model
{
    protected $table = 'portfolio_features';
}
