<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DbBackup extends Model
{
    protected $guarded = ['id'];
}
