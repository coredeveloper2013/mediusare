<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioTechnologyRelation extends Model
{
    protected $table = 'portfolio_technology_relations';


    public function technology(){
        return $this->belongsTo('App\Models\Technology', 'technology_id', 'id');
    }
}
