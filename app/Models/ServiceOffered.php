<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceOffered extends Model
{
    protected $table = 'service_offered';
}
