<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceTechnologyRelations extends Model
{
    protected $table = 'service_technology_relations';


    public function technology(){
        return $this->belongsTo('App\Models\Technology', 'technology_id', 'id');
    }


}
