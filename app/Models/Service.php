<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $fillable = [
        'sort'
    ];

    public function getTechnologies(){
        return $this->hasMany('App\Models\ServiceTechnologyRelations', 'service_id', 'id');
    }

    public function getServiceOffered(){
        return $this->hasMany('App\Models\ServiceOffered', 'service_id', 'id');
    }

    public function getServiceIndustries(){
        return $this->hasMany('App\Models\ServiceIndustries', 'service_id', 'id');
    }
}
